Install python

Open command line and execute:
Pip install pytest

Check pytest installation:
pytest --version

Pytest installed in:C:\Users\$(user)\AppData\Local\Programs\Python\Python39\Lib\site-packages

Install pyserial for serial port communication:
Pip install pyserial

Install PyTest Requests for Web UI and DM UI requests:
pip install pytest requests

Install pytest-jira module:
pip install pytest-jira

Pycharm IDE plugins for PyTest: