####################################################################################
# Filename:     coap_cli.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   05.03.2021
# Last update:  03.03.2021
#
# Version:      0.1
#
# Filetype:     Library file
# Description:  Functions to support coap encapsulation and writing to G3 CLI
# STATUS:       Under development
# Limitation:   File must be in the same location as scripts
####################################################################################


from cli import *
from resources_coap import *


def write_g3_coap(coap_msg, instance=1) -> bool:
    """
    Writes coap message to G3 CLI
    By default, instance is 1. May only differ in case of Lamp and Sensor objects
    Returns True if Success (read OK from CLI after sending command)
    """

    # Lamp and Sensor Objects need the instance to be processed
    if instance > 1:
        # build command, replacing '2/1' or '8/1' in original string, with '/instance'. format: 'coap 2/i, {payload}'
        data = 'coap ' + coap_msg[0].strip('1') + str(instance) + ', ' + coap_msg[1]
        print('\n', data)
    else:
        # build command to send. format: 'coap 1/1, {payload}'
        data = 'coap ' + coap_msg[0] + ', ' + coap_msg[1]
        print('\n', data)

    write_to_cli(data)

    response = read_from_cli()

    if '[COAP] OK' in response:
        print('\n[COAP] Write OK')
        return True
    else:
        return False


def send_g3_coap_factory_default() -> bool:
    """
    Sends the factory default coap command to G3 via CLI
    Closes Serial Port before the device does
    After 60 seconds, waits for CLI to become available again
    Returns True when Port is ready
    """
    if not write_g3_coap(coap_factDefault):
        print('\nFailed to write coap Factory Default!!')
        return False

    close_serial_port('cli')
    time.sleep(60)
    wait_g3_cli_ready()

    return True


def send_g3_coap_dali_reinit():
    """
    Sends the Dali Re-init command to G3 via CLI
    Returns False if the write fails
    """
    if write_g3_coap(coap_dali_reinit):
        return True
    else:
        print('\nFailed to write coap DALI Reinit!!')
        return False
    return


def send_g3_coap_reg_server_conf():
    """
    Sends the Registration server parameters to G3 via CLI
    Returns False if the write fails
    """
    if write_g3_coap(coap_rconf):
        return True
    else:
        print('\nFailed to write coap Registration Config Parameters!!')
        return False
    return


def send_g3_coap_reg_server_conf_trigg():
    """
    Sends the Registration server parameters with Registration trigger to G3 via CLI
    Returns False if the write fails
    """
    if write_g3_coap(coap_rconf_trig):
        return True
    else:
        print('\nFailed to write coap Registration Config Parameters with Reg Trigger!!')
        return False
    return


def send_g3_coap_re_registration():
    """
    Sends the Re-Registration command to G3 via CLI
    Returns False if the write fails
    """
    if write_g3_coap(coap_reReg):
        return True
    else:
        print('\nFailed to write coap Re-Registration!!')
        return False
    return


def send_g3_coap_proj_server_conf():
    """
    Sends the Project server parameters to G3 via CLI
    Returns False if the write fails
    """
    if write_g3_coap(coap_sconf):
        return True
    else:
        print('\nFailed to write coap Server Config Parameters!!')
        return False
    return


def send_g3_coap_gps_pos():
    """
    Sends the GPS Position defined in resources_generic.py parameters to G3 via CLI
    Returns False if the write fails
    """
    if write_g3_coap(coap_set_gps):
        return True
    else:
        print('\nFailed to write coap Server Config Parameters!!')
        return False
    return


def send_g3_coap_dimming_curve(instance=1):
    """
    Writes the dimming curve for a given instance on the G3 device via CLI
    Returns False if the write fails
    """

    # coap_dim_curve = ['2/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    #    "ConfID":1,"Queue":[{"Priority":64,"Dimming_curve":[100,100,100000,2,0,1111,1444,0.5,"1-10V",1,0,10,8]}]}}']



    # 'OversizeFactor', =
    # 'MaintenanceFactor', =
    # 'MaintenanceInterval', =
    # 'LowWattMinDim', =
    # 'LowWattMaxDim', =
    # 'HighWattMinDim', =
    # 'HighWattMaxDim', =
    # 'PowerFactorLimit = 0.5
    # 'WarmupTime',
    # 'LampType', = '1-10V' or 'DALI'
    # 'MinVoltage', only 1-10V
    # 'MaxVoltage', only 1-10V



    coap_dim_curve = ['2/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
        "ConfID":1,"Queue":[{"Priority":64,\
        "Dimming_curve":[100,100,100000,2,0,1111,1444,0.5,"1-10V",1,0,10,8]}]}}']

    if write_g3_coap(coap_dim_curve, instance):
        return True
    else:
        print('\nFailed to write coap Dimming profile!!')
        return False
    return


def send_g3_coap_sensor_conf(sensor_id, sensor_type, sensor_def_value, sensor_hold_time, senFDLsuo, instance=1):
    """
    Writes the sensor configuration for a given instance on the G3 device via CLI
    Returns False if the write fails
    """
    coap_sens_cfg = ['8/1', '{"Cfg": {"UTC": "' + UTC_TIME + '","DeviceID": "' + DeviceID + '",\
        "ConfID": 13,"Queue": [{"Priority": 64,"SensorGeneral": ["' + sensor_id + '","' + sensor_type + '",' + str(sensor_def_value) + ',' + str(sensor_hold_time) + ',' + senFDLsuo + ']}]}} ']

    if write_g3_coap(coap_sens_cfg, instance):
        return True
    else:
        print('\nFailed to write coap sensor configuration for instance: ', instance)
        return False
    return


def send_g3_firmware_update_settings():
    """
    Writes the firmware update and update notification server settings on the G3 device via CLI
    Returns False if any of the write actions is not successful
    """
    if not write_g3_coap(coap_fw_notif_server):
        print('\nFailed to write coap Firmware Update Notification Server Parameters!!')
        return False

    if not write_g3_coap(coap_fw_upd_apns):
        print('\nFailed to write coap Firmware Update Notification Server Parameters!!')
        return False

    else:
        return True  # both writes were OK


def send_g3_coap_update_app(update_coap_msg):
    """
    Sends the desired UpdateApp command to G3 via CLI
    Returns False if the write fails
    """
    if write_g3_coap(update_coap_msg):
        return True
    else:
        print('\nFailed to write coap Update App for Firmware upgrade!!')
        return False
    return


# def send_g3_coap_update_app():
#     """
#     Sends the UpdateApp command with variable fw_undertest to G3 via CLI
#     Returns False if the write fails
#     """
#     if write_g3_coap(coap_trig_upd):
#         return True
#     else:
#         print('\nFailed to write coap Update App for Firmware upgrade!!')
#         return False
#     return
#
#
# def send_g3_coap_downgrade_app():
#     """
#     Sends the UpdateApp command with variable fw_current to G3 via CLI
#     Returns False if the write fails
#     """
#     if write_g3_coap(coap_trig_dwgd):
#         return True
#     else:
#         print('\nFailed to write coap Update App Firmware downgrade!!')
#         return False
#     return
#
#
# def send_g3_coap_illegal_update_app():
#     """
#     Sends the UpdateApp command with variable fw_undertest to G3 via CLI
#     Returns False if the write fails
#     """
#     if write_g3_coap(coap_trig_fake_upd):
#         return True
#     else:
#         print('\nFailed to write coap Update App for Firmware upgrade!!')
#         return False
#     return
