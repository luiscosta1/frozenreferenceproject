# Author: Andrzej Jordan    
# Test script designed for G4 project
# created at 3.3.2020 
# last update 11.03.2020
# version 0.3 working in normal conditions

#STATUS: write and read works already, adding a readable form, logs in the file looks nice 
#problem: None - works as expecected
#LIMITATION: minicom must be closed, when you open in xed (default notepad) received log then you will see data in Chineese
# however you can open it in the LibreOffice Writer and it is displayed normally. 

#====================================================================================
#       This is a first test mainly to set proper log level 
#       and gather information about FW and HW versions
#====================================================================================

import serial
import time
import sys
import pytest
import logging

from cli import *   #home made library created to support G4 test execution

#====================================================================================
#       LOGGER to the console and file
#====================================================================================

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG) #possible logging levels are: DEBUG, INFO, WARNING, ERROR, CRITICAL

fileHandler = logging.FileHandler('logs/first_test ' + time.strftime("%Y:%m:%d_%H:%M:%S") + '.log')
fileHandler.setLevel(logging.DEBUG)

# define file handler and set formatter log to the file 
#formatter    = logging.Formatter('%(asctime)s : %(levelname)s : %(name)s : %(message)s') # with timestamp
#formatter    = logging.Formatter('%(levelname)s : %(name)s : %(message)s') #without timestamp
#formatter    = logging.Formatter('%(levelname)s : %(name)s : %(module)s : Line No %(lineno)s ::%(message)s') #with module name and line no

formatter    = logging.Formatter('%(levelname)s : %(name)s : %(funcName)s : %(message)s') #with func name 
fileHandler.setFormatter(formatter)

# add file handler to logger
logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.ERROR)

logger.addHandler(fileHandler)
logger.addHandler(consoleHandler)

logger.info('G4 test started: FIRST_TEST.py')

#====================================================================================

#@pytest.fixture()
def file_log(send, receive):
    """
    Logging information for possible tests result analysis.
    """
    logger.info('CLI command: ' + send)
    logger.info('Received message: ' + receive)

#====================================================================================
#           TEST DEFINITION
#====================================================================================
  
@pytest.mark.usefixtures("print_start")
def test_start():
    """
    Dummy test to remove any messages catched in the buffer. 
    """
    cli_command = 'loglevel print 3'
    expected_response = 'Global print log level set to 3'

    received_response = console_check(cli_command + '\r')
    analysed_response = response_check(received_response, cli_command)
    
    file_log(cli_command, received_response)
    assert 1==1

def test_loglevel():
    """
    Test checks if you can correctly change log level to limit number of unwanted messages on the console.
    """
    cli_command = 'loglevel print 2'
    expected_response = '\nGlobal print log level set to 2'

    received_response = console_check(cli_command + '\r')
    analysed_response = response_check(received_response, cli_command)
    
    logger.info('\n first_test.py-::-test_loglevel')
    file_log(cli_command, received_response)
    assert expected_response == analysed_response

@pytest.mark.skip
def test_loglevel_lamp():
    """
    Reducing ammount of unwanted logs.
    """
    cli_command = 'loglevel LAMP_S 4'
    expected_response = 'LAMP_S print log level set to 4'
    
    received_response = console_check(cli_command + '\r')
        
    logger.info('\n first_test.py-::-test_loglevel')
    file_log(cli_command, received_response)
    assert (expected_response in received_response)

@pytest.mark.skip
def test_loglevel_nrg():
    """
    Reducing ammount of unwanted logs.
    """
    cli_command = 'loglevel NRG_S 4'
    expected_response = 'NRG_S print log level set to 4'

    received_response = console_check(cli_command + '\r')
        
    logger.info('\n first_test.py-::-test_loglevel')
    file_log(cli_command, received_response)
    assert (expected_response in received_response)

def test_loglevel_rules():
    """
    Reducing ammount of unwanted logs.
    """
    cli_command = 'loglevel R_MNG 4'
    expected_response = 'R_MNG print log level set to 4'

    received_response = console_check(cli_command + '\r')
        
    logger.info('\n first_test.py-::-test_loglevel')
    file_log(cli_command, received_response)
    assert (expected_response in received_response)

def test_loglevel_relay():
    """
    Reducing ammount of unwanted logs.
    """
    cli_command = 'loglevel RELAY_S 4'
    expected_response = 'RELAY_S print log level set to 4'

    received_response = console_check(cli_command + '\r')
        
    logger.info('\n first_test.py-::-test_loglevel')
    file_log(cli_command, received_response)
    assert (expected_response in received_response)

def test_loglevel_light_dim():
    """
    Reducing ammount of unwanted logs.
    """
    cli_command = 'loglevel NRG_S 4'
    expected_response = 'NRG_S print log level set to 4'

    received_response = console_check(cli_command + '\r')
        
    logger.info('\n first_test.py-::-test_loglevel')
    file_log(cli_command, received_response)
    assert (expected_response in received_response)

def test_loglevel_lamp():
    """
    Reducing ammount of unwanted logs.
    """
    cli_command = 'loglevel LAMP_S 4'
    expected_response = 'LAMP_S print log level set to 4'

    received_response = console_check(cli_command + '\r')
        
    logger.info('\n first_test.py-::-test_loglevel')
    file_log(cli_command, received_response)
    assert (expected_response in received_response)

def test_loglevel_1_10V_S():
    """
    Reducing ammount of unwanted logs.
    """
    cli_command = 'loglevel 1_10V_S 4'
    expected_response = '1_10V_S print log level set to 4'

    received_response = console_check(cli_command + '\r')
        
    logger.info('\n first_test.py-::-test_loglevel')
    file_log(cli_command, received_response)
    assert (expected_response in received_response)

@pytest.mark.skip
def test_loglevel_photo():
    """
    Reducing ammount of unwanted logs.
    """
    cli_command = 'loglevel PHOTO_S 4'
    expected_response = 'PHOTO_S print log level set to 4'

    received_response = console_check(cli_command + '\r')
        
    logger.info('\n first_test.py-::-test_loglevel')
    file_log(cli_command, received_response)
    assert (expected_response in received_response)

@pytest.mark.skip
def test_hw_version():
    """
    Checking hardware version to log version which was used during tests.
    """
    cli_command = 'lwm2m read device 0 hardware_version'
    expected_response = 'Read string 3/0/18'
    
    received_response = console_check(cli_command + '\r')
    analysed_response = response_check(received_response, cli_command)
    
    trimmed_response = response_trim(analysed_response)
    value = response_get_value_str(analysed_response)
    print ('Used HARDWARE VERSION is:' + value)
    logger.warning('Used HARDWARE VERSION is:' + value)  

    file_log(cli_command, received_response)
    assert expected_response == trimmed_response
    
def test_fw_version():
    """
    Checking firmware version of the G4 device to log version which was used during tests.
    """
    cli_command = 'lwm2m read device 0 firmware_version'
    expected_response = 'Read string 3/0/3'
    
    received_response = console_check(cli_command + '\r')
    analysed_response = response_check(received_response, cli_command)
    
    trimmed_response = response_trim(analysed_response)
    value = response_get_value_str(analysed_response)
    print ('Used FIRMWARE VERSION is:' + value)
    logger.warning('Used FIRMWARE VERSION is:' + value)  

    file_log(cli_command, received_response)
    assert expected_response == trimmed_response

def test_lwm2m_read_manufacturer():
    """
    Check if mentioned element is readable.
    """
    cli_command = 'lwm2m read device 0 manufacturer'
    expected_response = "Read string 3/0/0: 'SCHREDER'"

    received_response = console_check(cli_command + '\r')
    
    file_log(cli_command, received_response)
    assert (expected_response in received_response)

def test_lwm2m_read_model_number():
    """
    Check if mentioned element is readable.
    """
    cli_command = 'lwm2m read device 0 model_number'
    expected_response = "Read string 3/0/1: 'G4'"

    received_response = console_check(cli_command + '\r')
    
    file_log(cli_command, received_response)
    assert (expected_response in received_response)

def test_lwm2m_read_serial_number():
    """
    Check if mentioned element is readable.
    """
    cli_command = 'lwm2m read device 0 serial_number'
    expected_response = "Read string 3/0/2: "

    received_response = console_check(cli_command + '\r')
    
    file_log(cli_command, received_response)
    assert (expected_response in received_response)

