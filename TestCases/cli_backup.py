####################################################################################
# Filename:     cli.py
# Author:       Andrzej Jordan
# Adapted by:   Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   02.03.2021
# Last Update:  04.03.2021
#
# Version:      0.1
#
# Filetype:     Library file
# Description:  Wrapper functions to support reading and writing G3 CLI and Logs
# Status:       Under development
# Limitation:   File must be in the same location as scripts
#               Re-think serial port opening.
####################################################################################

import serial
import pytest
import time

from resources_g3specific import *


# Serial port shall not be opened here. If the Device restarts, COM ports are closed and the program fails
serial_cli = serial.Serial(port=port_cli_port, baudrate=port_baudrate, timeout=port_timeout)        #'COM16'
#serial_logs = serial.Serial(port=port_logs_port, baudrate=port_baudrate, timeout=port_timeout)      #'COM20
#serial_binary = serial.Serial(port=port_binary_port, baudrate=port_baudrate, timeout=port_timeout)  #'COM18'


@pytest.fixture()
def print_start():
    print("")
    print("================================================================================")
    print("========               G3 TEST EXECUTION            ============================")
    print("========                                            ============================")
    print("========            Firmware Under Test:            ============================")
    print('========    START at:  ' + time.strftime("%Y:%m:%d %H:%M:%S") + '          ============================')
    print("================================================================================")
    print("")


def write_to_cli(cli_command) -> int:
    """
    Writes command to G3 CLI
    Returns number of bytes written
    """

    cli_command = cli_command + '\r'  # first, append \r (Enter) character to the command
    if serial_cli.isOpen():
        cli_command = cli_command.encode()
        bytes_written = serial_cli.write(cli_command)
    # serial_cli.flush()
    serial_cli.send_break(0.5)

    return bytes_written

def read_from_cli() -> str:
    """
    Reads command from G3 CLI
    Returns response
    """

    if serial_cli.isOpen():
        data = serial_cli.read_all().decode('UTF-8')
        data = trim_g3_response(data)
        return data


def trim_g3_response(data) -> str:
    """
    This is a auxiliary function, used to
    cut the received G3 response and returns only the desired response
    eg. Raw format: '\nRESPONSE\n[CLI]>' -> trimmed_response = RESPONSE
    """

    trimmed_response = data.strip('\n')                    # remove first '\n'
    trimmed_response = trimmed_response.strip('\n[CLI]>')  # search and remove '\n[CLI]>'

    return trimmed_response


def read_from_logs(max_characters) -> str:
    """
    Reads command from G3 logs until max_characters argument is reached
    Returns the response
    """

    data = ''
    if serial_logs.isOpen():
        while True:
            #  Append read data to the buffer until it reaches max_characters
            data += serial_logs.readline().decode('UTF-8')
            if len(data) > max_characters:
                break
    return data


def wait_cli_ready(port_cli_port) -> bool:
    """
    After G3 Reboot
    Wait for console until [CLI]> will appear at the new line.
    This means it is ready to start receiving commands
    Returns True when '[CLI]>' is found in the buffer
    ********  NOT YET READY  ********
    """

    data = ''
    while True:
        if not serial_cli.isOpen():  # Restart closes COM Port. We need to wait until it re-opens
            time.sleep(1.0)
            continue
        else:                       # Port is now open
            while True:
                data += serial_cli.readline().decode('UTF-8')
                if print('[CLI]>' not in data):
                    time.sleep(1.0)
                    continue
                else:
                    return True








# @pytest.fixture()
def response_trim(response):
    """
    Function to cut the received response and return parameter name without stored value.
    """
    # print ('Response to trim: ' + response)
    i = 0
    i = response.find(':')
    trimmed_response = response[1:i]
    # print ('Read parameter: ' + trimmed_response + ' has got value: ' + response[i:len(response)])

    return trimmed_response


# @pytest.fixture()
def response_get_value_str(response):
    """
    Function to cut the received response and return value of the parameter.
    """
    # print ('Response to trim: ' + response)
    i = 0
    response_end = response[-30:-1]
    i = response_end.find(':')
    j = response_end.find('\r')
    response_value = response_end[i + 1:j]
    print('Readed value: ' + response_value)
    return response_value


def response_get_value2(response, text):
    """
    Extension from the get value function to seek the response in the large response.
    """
    i = 0

    response_end = response[-100:-1]
    i = response_end.find(text)
    response_cut = response_end[i:-1]
    j = response_cut.find('\r')
    text_length = len(text)

    response_value = response_end[i + text_length:i + j]
    print('Readed value: ' + response_value)
    return response_value


def response_get_value_float(response, text):
    """
    Function to change the received response and return value as the float.
    """
    response_value = response_get_value2(response, text)
    print('Response value is: ' + str(response_value))

    response_value_float = float(response_value)
    print('Response value number is: ' + str(response_value_float))

    return response_value_float


def response_get_value(response):
    """
    Function to change the received response and return value as the float.
    """
    response_value = response_get_value_str(response)
    # print ('Response value is: ' + str(response_value))

    response_value_float = float(response_value)
    # print ('Response value number is: ' + str(response_value_float))

    return response_value_float


# Needs adaptation to G3!
def console_check(command):
    """
    Function to send a command to the CLI console .
    for G4, it prints the sent command just sent. this "response" is not the actual response  to the command
    Needs further adaptation!
    """
    if serial_cli.isOpen():
        # print(cli.name + ' is open.')
        # print('\nCommand sent to CLI console: ' + command)
        b_command = command.encode()  # in the Python3 you need encode data before sending it on the console
        serial_cli.write(b_command)  # execute CLI command on the debug console
        response = read_response(serial_cli)
        print('  Debug console: \n' + response)

    return response


# @pytest.fixture()
def response_check(response, command):
    """
    Function to cut the response and remove sent command. The aim is to return the response from the G4 device.
    """
    response_length = len(response)
    command_length = len(command)
    response = response[command_length + 2:response_length - 4]
    # print('The response is: ' + response)

    return response


def send(command):
    """
    Function to send a command to the CLI console .
    """
    if serial_cli.isOpen():
        # print(cli.name + ' is open.')
        # print('\nCommand sent to CLI console: ' + command)
        b_command = command.encode()  # in the Python3 you need encode data before sending it on the console
        serial_cli.write(b_command)  # execute CLI command on the debug console
        serial_cli.flush()


def read_console():
    """
    Function to read the CLI console .
    """
    if serial_cli.isOpen():
        response = read_response(serial_cli)
        print('  Debug console: \n' + response)
        return response
