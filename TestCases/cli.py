####################################################################################
# Filename:     cli.py
# Author:       Andrzej Jordan
# Adapted by:   Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   02.03.2021
# Last Update:  04.03.2021
#
# Version:      0.1
#
# Filetype:     Library file
# Description:  Wrapper functions to support reading and writing G3 CLI and Logs
# Status:       Under development
# Limitation:   File must be in the same location as scripts
#               Re-think serial port opening.
####################################################################################

import serial
import pytest
import time
import datetime

from resources_g3specific import *

# Declare serial ports as global variables, but do not open them
serial_cli = serial.Serial()
serial_cli.port = port_cli_port
serial_cli.baudrate = port_baudrate
serial_cli.timeout = port_timeout

serial_logs = serial.Serial()
serial_logs.port = port_logs_port
serial_logs.baudrate = port_baudrate
serial_logs.timeout = port_timeout

serial_binary = serial.Serial()
serial_binary.port = port_binary_port
serial_binary.baudrate = port_baudrate
serial_binary.timeout = port_timeout


@pytest.fixture()
def print_start():
    print("")
    print("================================================================================")
    print("========               G3 TEST EXECUTION            ============================")
    print("========                                            ============================")
    print("========            Firmware Under Test:            ============================")
    print('========    START at:  ' + time.strftime("%Y:%m:%d %H:%M:%S") + '          ============================')
    print("================================================================================")
    print("")

def close_serial_port(port):
    """
    Closes the specified serial port
    """
    if port == 'cli':
        serial_cli.close()
    elif port == 'logs':
        serial_logs.close()
    elif port == 'binary':
        serial_binary.close()
    else:
        return  #  nothing to do

def write_to_cli(cli_command) -> int:
    """
    Writes command to G3 CLI
    Returns number of bytes written
    """

    cli_command = cli_command + '\r'  # first, append \r (Enter) character to the command

    if not serial_cli.isOpen():
        serial_cli.open()

    cli_command = cli_command.encode()
    bytes_written = serial_cli.write(cli_command)
    # serial_cli.flush()
    serial_cli.send_break(0.5)

    return bytes_written


def read_from_cli() -> str:
    """
    Reads command from G3 CLI
    Returns response
    """

    if not serial_cli.isOpen():
        serial_cli.open()

    data = serial_cli.read_all().decode('UTF-8')
    data = trim_g3_response(data)
    return data


def trim_g3_response(data) -> str:
    """
    This is a auxiliary function, used to
    cut the received G3 response and returns only the desired response
    eg. Raw format: '\nRESPONSE\n[CLI]>' -> trimmed_response = RESPONSE
    """

    trimmed_response = data.strip("[CLI]>")
    trimmed_response = trimmed_response.strip('\n')  # remove both '\n'

    return trimmed_response


def read_from_g3_logs(max_characters) -> str:
    """
    Reads command from G3 logs until max_characters argument is reached
    Returns the response
    """

    data = ''
    if not serial_logs.isOpen():
        serial_logs.open()

    while True:
        #  Append read data to the buffer until it reaches max_characters
        data += serial_logs.readline().decode('UTF-8')
        if len(data) > max_characters:
            break
    return data


def set_g3_time(str_time):
    """
    Writes the received time in G3 CLI
    time shall be specified in YYMMDDhhmmss format. Otherwise, response will not be ok
    Returns False if response does not contain '[TIME]'
    """
    command = 'time ' + str_time

    write_to_cli(command)
    response = read_from_cli()
    if '[TIME]' not in response:
        print('Could not write time in device')
        return False

    return True


def set_g3_gps(latitude, longitude, accuracy=1):
    """
    Writes the received coordinates in G3 CLI
    If not specified, accuracy is set to 1 by default
    Returns False if response is not 'OK'
    """

    command = 'set gps ' + str(latitude) + ' ' + str(longitude) + ' ' + str(accuracy)  # set gps <lat> <long> <accuracy>
    write_to_cli(command)
    response = read_from_cli()
    if not response == 'OK':
        print('Could not write gps position in device')
        return False

    return True


def wait_g3_cli_ready() -> bool:
    """
    After G3 Reboot
    Wait for console until [CLI]> will appear at the new line.
    This means it is ready to start receiving commands
    Returns True when '[CLI]>' is found in the buffer.
    Returns False if for 5 minutes it didn't show up
    ********  NOT YET READY  ********
    """

    data = ''
    while not serial_cli.isOpen():  # Restart closes COM Port. We need to wait and retry until it re-opens
        serial_cli.open()
        time.sleep(1.0)
        continue

    starttime_plus_5 = datetime.datetime.now() + datetime.timedelta(minutes=5)

    # Port is now open.
    while datetime.datetime.now() < starttime_plus_5:   # for a max of 5 minutes
        data += serial_cli.read_all().decode('UTF-8')   # check if '[CLI]>' shows up in the cli
        if '[CLI]>' not in data:
            time.sleep(1.0)
            continue
        else:
            print('CLI is ready to go!')
            return True
    # Something went wrong
    print('CLI was not ready in 5 minutes')
    return False


# @pytest.fixture()
def response_trim(response):
    """
    Function to cut the received response and return parameter name without stored value.
    """
    # print ('Response to trim: ' + response)
    i = 0
    i = response.find(':')
    trimmed_response = response[1:i]
    # print ('Read parameter: ' + trimmed_response + ' has got value: ' + response[i:len(response)])

    return trimmed_response


# @pytest.fixture()
def response_get_value_str(response):
    """
    Function to cut the received response and return value of the parameter.
    """
    # print ('Response to trim: ' + response)
    i = 0
    response_end = response[-30:-1]
    i = response_end.find(':')
    j = response_end.find('\r')
    response_value = response_end[i + 1:j]
    print('Readed value: ' + response_value)
    return response_value


def response_get_value2(response, text):
    """
    Extension from the get value function to seek the response in the large response.
    """
    i = 0

    response_end = response[-100:-1]
    i = response_end.find(text)
    response_cut = response_end[i:-1]
    j = response_cut.find('\r')
    text_length = len(text)

    response_value = response_end[i + text_length:i + j]
    print('Readed value: ' + response_value)
    return response_value


def response_get_value_float(response, text):
    """
    Function to change the received response and return value as the float.
    """
    response_value = response_get_value2(response, text)
    print('Response value is: ' + str(response_value))

    response_value_float = float(response_value)
    print('Response value number is: ' + str(response_value_float))

    return response_value_float


def response_get_value(response):
    """
    Function to change the received response and return value as the float.
    """
    response_value = response_get_value_str(response)
    # print ('Response value is: ' + str(response_value))

    response_value_float = float(response_value)
    # print ('Response value number is: ' + str(response_value_float))

    return response_value_float


# Needs adaptation to G3!
def console_check(command):
    """
    Function to send a command to the CLI console .
    for G4, it prints the sent command just sent. this "response" is not the actual response  to the command
    Needs further adaptation!
    """
    if serial_cli.isOpen():
        # print(cli.name + ' is open.')
        # print('\nCommand sent to CLI console: ' + command)
        b_command = command.encode()  # in the Python3 you need encode data before sending it on the console
        serial_cli.write(b_command)  # execute CLI command on the debug console
        response = read_response(serial_cli)
        print('  Debug console: \n' + response)

    return response


# @pytest.fixture()
def response_check(response, command):
    """
    Function to cut the response and remove sent command. The aim is to return the response from the G4 device.
    """
    response_length = len(response)
    command_length = len(command)
    response = response[command_length + 2:response_length - 4]
    # print('The response is: ' + response)

    return response


def send(command):
    """
    Function to send a command to the CLI console .
    """
    if serial_cli.isOpen():
        # print(cli.name + ' is open.')
        # print('\nCommand sent to CLI console: ' + command)
        b_command = command.encode()  # in the Python3 you need encode data before sending it on the console
        serial_cli.write(b_command)  # execute CLI command on the debug console
        serial_cli.flush()


def read_console():
    """
    Function to read the CLI console .
    """
    if serial_cli.isOpen():
        response = read_response(serial_cli)
        print('  Debug console: \n' + response)
        return response
