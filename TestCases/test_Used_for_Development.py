#local cli.py library already imports serial library
from cli import *
from resources_generic import *
from coap_e2e import *
from http_requests_azure import *

#@pytest.mark.skip
def test_read_firmware_version():
    """Requests and verifies if Firmware version is the expected"""
    cli_command = 'version'
    write_to_cli(cli_command)
    response = read_from_cli()
    assert fw_current in response
    print (response)
    pass

@pytest.mark.skip
def test_wait_cli_ready():
    """
    Sends Restart command to g3 via cli
    Closes Serial Port before the device does
    After 60 seconds, waits for CLI to become available again
    Returns True when Port is ready
    """

    cli_command = 'restart'

    success = write_to_cli(cli_command)
    assert success, '\nFailed to write restart command!!'

    close_serial_port('cli')
    time.sleep(60)
    ready = wait_g3_cli_ready()
    assert ready

    pass


def test_e2e_coap():
    success = send_e2e_coap_dali_reinit()
    assert success, "something went wrong"


def test_acquire_token_from_credentials():

    token = get_azure_ad_token()

    print('\n token = ', token)

