####################################################################################
# Filename:     http_requests_jira.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   22.03.2021
# Last update:  22.03.2021
#
# Version:      0.1
#
# Filetype:     Library file
# Description:  Functions to support HTTP requests to Jira
# STATUS:       New
# Limitation:   Need to change the request functions to be more generic
#               Eventually create a function for Post, another for request.... to be evaluated
####################################################################################

import requests
from requests.auth import HTTPBasicAuth
import json
from resources_generic import *


# This has to be more generic
def jira_request(url, request_type, payload=''):
    """
    Sends the received request to Jira
    Returns the received response
    payload is optional, eg. for GET requests
    POST requests are TO BE implemented
    """

    auth = HTTPBasicAuth(jira_user, jira_token)

    headers = {
        "Accept": "application/json"
    }

    # query = {
    #     'query': 'query'
    # }

    response = requests.request(
        request_type,
        url,
        headers=headers,  # params=query,  (optional)
        auth=auth
    )

    return response

