####################################################################################
# Filename:     test_e2e_Owlet_to_Exedra_Migration_TST_595.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   15.03.2021
# Last update:  15.03.2021
#
# Version:      0.1
#
# Filetype:     Test Case file
# Description:  Test Case for device Migration from Owlet IoT to Exedra [TST-595]
# STATUS:       Stable
# Limitation:
####################################################################################

from coap_cli import *
from datapoints import *
from http_requests import *
from http_requests_exedra import *
from logs import *
import time



#@pytest.mark.e2e
def test_e2e_owlet_to_exedra_migration():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)


    # set device time to local UTC_TIME
    if not set_g3_time(UTC_TIME):
        return
    logger.info("UTC Time written via CLI Coap")

    # send sconf via cli coap
    send_g3_coap_proj_server_conf()

    # confirm sconf datapoints on the device
    response = read_datapoint('APN')
    expected_response = apn_url
    assert response == expected_response

    response = read_datapoint('APNUser')
    expected_response = apn_usr
    assert response == expected_response

    response = read_datapoint('APNPW')
    expected_response = apn_pwd
    assert response == expected_response

    response = read_datapoint('SURL')
    expected_response = SURL + '.1'
    assert response == expected_response

    response = read_datapoint('Suser')
    expected_response = ''
    assert response == expected_response

    response = read_datapoint('Spw')
    expected_response = ''
    assert response == expected_response

    logger.info("Project server parameters successfully written via CLI Coap")

    # send rconf via cli coap
    send_g3_coap_reg_server_conf()

    # confirm rconf datapoints on the device
    response = read_datapoint('RAPN')
    expected_response = apn_url
    assert response == expected_response

    response = read_datapoint('RAPNUser')
    expected_response = apn_usr
    assert response == expected_response

    response = read_datapoint('RAPNPW')
    expected_response = apn_pwd
    assert response == expected_response

    response = read_datapoint('RSURL')
    expected_response = RURL + '.1'
    assert response == expected_response

    response = read_datapoint('RSuser')
    expected_response = ''
    assert response == expected_response

    response = read_datapoint('RSpw')
    expected_response = ''
    assert response == expected_response

    # send gps coap              # - The device is processing the coap message but for some reason it is not applying
    # send_coap_gps_pos()

    # force gps cli command for now
    set_g3_gps(gps_lat, gps_long)

    # check gps datapoints
    response = read_datapoint('POSLAT')
    expected_response = gps_lat
    assert response == expected_response

    response = read_datapoint('POSLON')
    expected_response = gps_long
    assert response == expected_response

    logger.info("GPS Coordinates successfully written via CLI Coap")

    # send coap_reReg

    assert send_g3_coap_re_registration(), "Failed to write Re-Registration message via CLI Coap"

    logger.info("Re-Registration message successfully written via CLI Coap")

    logger.info("Waiting 2 minutes for registration")
    # wait 2 mins
    time.sleep(2 * 60)

    # confirm device shows up in IoT HUB
    device_in_iot_hub = dm_ui_request_device_id(DeviceID)
    assert device_in_iot_hub, "Device not in IoT HUB!"
    logger.info("Device is present in IoT HUB")

    # confirm device shows up in Exedra
    device_in_exedra = exedra_request_device_id(DeviceID)
    assert device_in_exedra, "Device not in Exedra!"
    logger.info("Device is present in Exedra")

    # confirm server sconf datapoints on the device
    response = read_datapoint('APN')
    expected_response = apn_url
    assert response == expected_response

    response = read_datapoint('APNUser')
    expected_response = apn_usr
    assert response == expected_response

    response = read_datapoint('APNPW')
    expected_response = apn_pwd
    assert response == expected_response

    response = read_datapoint('SURL')
    expected_response = SURL + '.1'
    assert response == expected_response

    response = read_datapoint('Suser')
    expected_response = ''
    assert response == expected_response

    response = read_datapoint('Spw')
    expected_response = ''
    assert response == expected_response

    logger.info("Project Server configurations correctly set on the Device")

    # read Registration status on the G3 Device.
    # Takes too long to change to 2. Assertion fails even with 4 min sleep
    expected_reg_state = 2
    starttime_plus_5 = datetime.datetime.now() + datetime.timedelta(minutes=5)

    while datetime.datetime.now() < starttime_plus_5:  # for a max of 5 minutes
        response = read_datapoint('RegnState')
        if response != expected_reg_state:
            continue
        else:
            logger.info("Registration state = 2")
            break

    print('RegnState =', response)
    assert response == expected_reg_state

    logger.info("Registration state = " + str(response))

    pass
