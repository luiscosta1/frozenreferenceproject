####################################################################################
# Filename:     test_request_to_exedra.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   05.03.2021
# Last update:  05.03.2021
#
# Version:      0.1
#
# Filetype:     Test Case file
# Description:  Unit Test Cases to Request the Device info from Device Management
# STATUS:       New
# Limitation:
####################################################################################


from http_requests_exedra import *
from resources_generic import *
import json
import pytest
import logging
import time
from logs import *

# logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)  # DEBUG or INFO or WARNING or ERROR or CRITICAL
#
# fileHandler = logging.FileHandler('logs/' + __name__ + ' ' + time.strftime("%Y %m %d-%H %M %S") + '.log')
# fileHandler.setLevel(logging.DEBUG)
#
# formatter = logging.Formatter('%(levelname)s : %(name)s : %(funcName)s : %(message)s')  # with func name
# fileHandler.setFormatter(formatter)
#
# # add file handler to logger
# logger.addHandler(fileHandler)
#
# consoleHandler = logging.StreamHandler()
# consoleHandler.setLevel(logging.ERROR)
#
# logger.addHandler(fileHandler)
# logger.addHandler(consoleHandler)


def test_exedra_request():
    """
    Requests All devices registered in TST IoT HUB

    Although the returned "counts" is a higher value, it only shows the first 100
    To request All devices, we may need to use the nextPageToken returned in the response"""

    url = "https://" + dm_ui_url + "/api/v1/device-hubs/" + dm_ui_hub + "/devices"

    response = exedra_request(url, 'GET')

    print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))


# @pytest.mark.skipif() shall be skipped if test_dm_ui_request fails
def test_exedra_request_DeviceID():
    """
    Checks if a given device is registered in Exedra
    """

    device_present = exedra_request_device_id(DeviceID)

    assert device_present is True, "Device not Present in Exedra"

    print('\nDevice ', DeviceID, ' is present in Exedra!')


def test_exedra_request_device_firmware_version():
    """
    Requests the device firmware version in Exedra
    """

    fw_read = exedra_request_device_properties(DeviceID, 'firmwareVersion')

    assert fw_read == fw_undertest, "Firmware versions Mismatch in Exedra!"

    print('\nDevice Firmware in Exedra: ', fw_read)


def test_exedra_request_all_device_properties():
    """
    Requests all the device Properties
    """

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    print('\nDevice Core:')
    logger.info('\nDevice Core:')
    device_property = exedra_request_device_properties(DeviceID, 'type')
    logger.info('type: ' + device_property)
    print('type: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'groupPath')
    logger.info('groupPath: ' + device_property)
    print('groupPath: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'unmanaged')
    logger.info('unmanaged: ' + str(device_property))
    print('unmanaged: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'simulated')
    logger.info('simulated: ' + str(device_property))
    print('simulated: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'commissioning')
    logger.info('hasCommissioning: ' + str(device_property['hasCommissioning']))
    logger.info('commissioned: ' + str(device_property['commissioned']))
    logger.info('status: ' + device_property['status'])
    print('hasCommissioning: ', device_property['hasCommissioning'])
    print('commissioned: ', device_property['commissioned'])
    print('status: ', device_property['status'])
    device_property = exedra_request_device_properties(DeviceID, 'errors')
    if device_property is not None:
        logger.info('errors: ' + device_property['status'])
        print('errors: ', device_property['status'])
    device_property = exedra_request_device_properties(DeviceID, 'updates')
    logger.info('updates: ' + str(device_property))
    print('updates: ', device_property)
    if device_property is not None:
        logger.info('updates fields: ' + device_property['fields'])
        logger.info('updates reasons: ' + device_property['reasons'])
        print('updates fields: ', device_property['fields'])
        print('updates reasons: ', device_property['reasons'])
    device_property = exedra_request_device_properties(DeviceID, 'schemaId')
    logger.info('schemaId: ' + device_property)
    print('schemaId: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'model')
    logger.info('model: ' + device_property)
    print('model: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'address')
    if device_property is not None:
        logger.info('address city: ' + device_property['city'])
        logger.info('address country: ' + device_property['country'])
        print('address city: ', device_property['city'])
        print('address country: ', device_property['country'])
        # print('address street_address: ', device_property['street_address'])
        #print('address zipcode: ', device_property['zipcode'])
    device_property = exedra_request_device_properties(DeviceID, 'activeFailures')
    logger.info('activeFailures: ' + str(device_property))
    print('activeFailures: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'systematics')
    logger.info('systematics: ' + str(device_property))
    print('systematics: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'labels')
    logger.info('labels: ' + str(device_property))
    print('labels: ', device_property)

    print('\nDevice Coords:')
    logger.info('\nDevice Coords:')
    device_property = exedra_request_device_properties(DeviceID, 'coordinates')
    logger.info('latitude: ' + str(device_property[0]))
    logger.info('longitude: ' + str(device_property[1]))
    print('latitude: ', device_property[0])
    print('longitude: ', device_property[1])

    print('\nDevice Attributes:')
    logger.info('\nDevice Attributes:')
    device_property = exedra_request_device_properties(DeviceID, 'calendarName')
    logger.info('calendarName: ' + device_property)
    print('calendarName: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'lampWattage')
    logger.info('lampWattage: ' + str(device_property))
    print('lampWattage: ', device_property)

    print('\nDevice Failures:')
    logger.info('\nDevice Failures:')
    device_property = exedra_request_device_properties(DeviceID, 'connectionLost')
    logger.info('connectionLost: ' + str(device_property['value']))
    logger.info('connectionLost date: ' + str(device_property['date']))
    print('connectionLost: ', device_property['value'])
    print('connectionLost date: ', device_property['date'])
    device_property = exedra_request_device_properties(DeviceID, 'communicationFailure')
    logger.info('communicationFailure: ' + str(device_property['value']))
    logger.info('communicationFailure date: ' + str(device_property['date']))
    print('communicationFailure: ', device_property['value'])
    print('communicationFailure date: ', device_property['date'])
    device_property = exedra_request_device_properties(DeviceID, 'lampFailure')
    logger.info('lampFailure: ' + str(device_property['value']))
    logger.info('lampFailure date: ' + str(device_property['date']))
    print('lampFailure: ', device_property['value'])
    print('lampFailure date: ', device_property['date'])
    device_property = exedra_request_device_properties(DeviceID, 'lampPowerTooHigh')
    logger.info('lampPowerTooHigh: ' + str(device_property['value']))
    logger.info('lampPowerTooHigh date: ' + str(device_property['date']))
    print('lampPowerTooHigh: ', device_property['value'])
    print('lampPowerTooHigh date: ', device_property['date'])
    device_property = exedra_request_device_properties(DeviceID, 'lampPowerTooLow')
    logger.info('lampPowerTooLow: ' + str(device_property['value']))
    logger.info('lampPowerTooLow date: ' + str(device_property['date']))
    print('lampPowerTooLow: ', device_property['value'])
    print('lampPowerTooLow date: ', device_property['date'])
    device_property = exedra_request_device_properties(DeviceID, 'powerFactorTooLow')
    logger.info('powerFactorTooLow: ' + str(device_property['value']))
    logger.info('powerFactorTooLow date: ' + str(device_property['date']))
    print('powerFactorTooLow: ', device_property['value'])
    print('powerFactorTooLow date: ', device_property['date'])
    device_property = exedra_request_device_properties(DeviceID, 'highTemperature')
    if device_property is not None:
        logger.info('highTemperature: ' + str(device_property['value']))
        logger.info('highTemperature date: ' + str(device_property['date']))
        print('highTemperature: ', device_property['value'])
        print('highTemperature date: ', device_property['date'])
    device_property = exedra_request_device_properties(DeviceID, 'supplyVoltageTooHigh')
    logger.info('supplyVoltageTooHigh: ' + str(device_property['value']))
    logger.info('supplyVoltageTooHigh date: ' + str(device_property['date']))
    print('supplyVoltageTooHigh: ', device_property['value'])
    print('supplyVoltageTooHigh date: ', device_property['date'])
    device_property = exedra_request_device_properties(DeviceID, 'supplyVoltageTooLow')
    logger.info('supplyVoltageTooLow: ' + str(device_property['value']))
    logger.info('supplyVoltageTooLow date: ' + str(device_property['date']))
    print('supplyVoltageTooLow: ', device_property['value'])
    print('supplyVoltageTooLow date: ', device_property['date'])

    print('\nDevice Configuration:')
    logger.info('\nDevice Configuration:')
    device_property = exedra_request_device_properties(DeviceID, 'deviceId')
    logger.info('deviceId: ' + device_property)
    print('deviceId: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'TimeZoneId')
    logger.info('TimeZoneId: ' + device_property)
    print('TimeZoneId: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'firmwareVersion')
    logger.info('firmwareVersion: ' + device_property)
    print('firmwareVersion: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'imsi')
    logger.info('imsi: ' + device_property)
    print('imsi: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'imei')
    logger.info('imei: ' + device_property)
    print('imei: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'lastSuccessfulCommunication')
    logger.info('lastSuccessfulCommunication: ' + device_property)
    print('lastSuccessfulCommunication: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'networkOperator')
    logger.info('networkOperator: ' + device_property)
    print('networkOperator: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'assetId')
    logger.info('assetId: ' + device_property)
    print('assetId: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'dimmingType')
    logger.info('dimmingType: ' + device_property)
    print('dimmingType: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'daliIndex')
    logger.info('daliIndex: ' + device_property)
    print('daliIndex: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'daliCount')
    logger.info('daliCount: ' + str(device_property))
    print('daliCount: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'catalogId')
    logger.info('catalogId: ' + device_property)
    print('catalogId: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'powerFactorThreshold')
    logger.info('powerFactorThreshold: ' + str(device_property))
    print('powerFactorThreshold: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'minVoltageSignal')
    logger.info('minVoltageSignal: ' + str(device_property))
    print('minVoltageSignal: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'minDimValue')
    logger.info('minDimValue: ' + str(device_property))
    print('minDimValue: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'maxOutputPower')
    logger.info('maxOutputPower: ' + str(device_property))
    print('maxOutputPower: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'maintenanceFactor')
    logger.info('maintenanceFactor: ' + str(device_property))
    print('maintenanceFactor: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'maintenanceInterval')
    logger.info('maintenanceInterval: ' + str(device_property))
    print('maintenanceInterval: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'lowEnergyLimitMinDim')
    logger.info('lowEnergyLimitMinDim: ' + str(device_property))
    print('lowEnergyLimitMinDim: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'lowEnergyLimitMaxDim')
    logger.info('lowEnergyLimitMaxDim: ' + str(device_property))
    print('lowEnergyLimitMaxDim: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'highEnergyLimitMinDim')
    logger.info('highEnergyLimitMinDim: ' + str(device_property))
    print('highEnergyLimitMinDim: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'highEnergyLimitMaxDim')
    logger.info('highEnergyLimitMaxDim: ' + str(device_property))
    print('highEnergyLimitMaxDim: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'sensorHoldTime')
    logger.info('sensorHoldTime: ' + str(device_property))
    print('sensorHoldTime: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'luminaireModel')
    logger.info('luminaireModel: ' + device_property)
    print('luminaireModel: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'driverName')
    logger.info('driverName: ' + device_property)
    print('driverName: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'numberOfLeds')
    logger.info('numberOfLeds: ' + str(device_property))
    print('numberOfLeds: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'ledColourTemperature')
    logger.info('ledColourTemperature: ' + device_property)
    print('ledColourTemperature: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'luminaireColour')
    logger.info('luminaireColour: ' + device_property)
    print('luminaireColour: ', device_property)
    device_property = exedra_request_device_properties(DeviceID, 'temperatureThreshold')
    logger.info('temperatureThreshold: ' + str(device_property))
    print('temperatureThreshold: ', device_property)

    print('\nDevice Metering:')
    logger.info('\nDevice Metering:')
    device_property = exedra_request_device_properties(DeviceID, 'activeEnergy')
    logger.info('activeEnergy: ' + str(device_property['value']))
    print('activeEnergy: ', device_property['value'])
    device_property = exedra_request_device_properties(DeviceID, 'activePower')
    logger.info('activePower: ' + str(device_property['value']))
    print('activePower: ', device_property['value'])
    device_property = exedra_request_device_properties(DeviceID, 'actualLightState')
    logger.info('actualLightState: ' + str(device_property['value']))
    print('actualLightState: ', device_property['value'])
    device_property = exedra_request_device_properties(DeviceID, 'operatingHours')
    logger.info('operatingHours: ' + str(device_property['value']))
    print('operatingHours: ', device_property['value'])
    device_property = exedra_request_device_properties(DeviceID, 'powerFactor')
    logger.info('powerFactor: ' + str(device_property['value']))
    print('powerFactor: ', device_property['value'])
    device_property = exedra_request_device_properties(DeviceID, 'supplyCurrent')
    logger.info('supplyCurrent: ' + str(device_property['value']))
    print('supplyCurrent: ', device_property['value'])
    device_property = exedra_request_device_properties(DeviceID, 'supplyVoltage')
    logger.info('supplyVoltage: ' + str(device_property['value']))
    print('supplyVoltage: ', device_property['value'])
    device_property = exedra_request_device_properties(DeviceID, 'lampCommandMode')
    logger.info('lampCommandMode: ' + str(device_property['value']))
    print('lampCommandMode: ', device_property['value'])
    device_property = exedra_request_device_properties(DeviceID, 'targetLightCommand')
    logger.info('targetLightCommand: ' + str(device_property['value']))
    print('targetLightCommand: ', device_property['value'])
    device_property = exedra_request_device_properties(DeviceID, 'switchOnCounter')
    if device_property is not None:
        logger.info('switchOnCounter: ' + str(device_property['value']))
        print('switchOnCounter: ', device_property['value'])
