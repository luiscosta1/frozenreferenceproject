####################################################################################
# Filename:     test_request_to_jira.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   05.03.2021
# Last update:  08.03.2021
#
# Version:      0.1
#
# Filetype:     Test Case file
# Description:  Just a simple test requesting info on a Jira Ticket
# STATUS:       New
# Limitation:
####################################################################################


from http_requests_jira import *
import json
import logging
import time

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)  # DEBUG or INFO or WARNING or ERROR or CRITICAL

fileHandler = logging.FileHandler('logs/' + __name__ + ' ' + time.strftime("%Y%m%d%H%M%S") + '.log')
fileHandler.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(levelname)s : %(name)s : %(funcName)s : %(message)s')  # with func name
fileHandler.setFormatter(formatter)

# add file handler to logger
logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.ERROR)

logger.addHandler(fileHandler)
logger.addHandler(consoleHandler)


def test_jira_request():
    url = jira_url + '/rest/api/3/issue/TST-687'

    response = jira_request(url, 'GET')

    print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))
    logger.info('Response:' + response.text)
