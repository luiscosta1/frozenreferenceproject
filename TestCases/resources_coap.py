####################################################################################
# Filename:     resources_coap.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   05.03.2021
# Last update:  05.03.2021
#
# Version:      0.1
#
# Filetype:     Resource file
# Description:  Coap messages to support test scripts for the G3 project
# Status:       Under development
# Limitation:   File must be in the same location as scripts
####################################################################################

from enum import Enum
from resources_generic import *
import time
import datetime


# G3 Datapoint Objects
class G3objects(Enum):
    G3_Controller = 1
    G3_Lamp = 2
    G3_PositionTime = 3
    G3_ACPower = 4
    G3_Comms = 5
    G3_DataInfo = 6
    G3_Management = 7
    G3_Sensor = 8
    G3_CLDriver = 9


UTC_TIME = time.strftime("%y%m%d%H%M%S")

# Get current time + 5 min in "ss mm HH DD MM * YYYY" format. Used for manual dimming profile
now_plus_5 = datetime.datetime.now() + datetime.timedelta(minutes=5)
UTC_TIME_plus_5min = now_plus_5.strftime("%S %M %H %d %m * %Y")

############  Controller datapoints -> 1/1  ############
coap_factDefault = ['1/1', '{"Ctrl":{"UTC":"' + UTC_TIME + '","DeviceID": "' + DeviceID + '",\
    "ContID":8,"Queue":[{"Priority":64,"FACTdef":true}]}}']

coap_reReg = ['1/1', '{"Ctrl":{"UTC":"' + UTC_TIME + '","DeviceID": "' + DeviceID + '",\
    "ContID":8,"Queue":[{"Priority":64,"Re-register":true}]}}']

coap_dali_reinit = ['1/1', '{"Ctrl":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ContID":2,"Queue":[{"Priority":64,"DALIre-init":true}]}}']

coap_loc_sen_en = ['1/1', '{"Cfg": {"UTC": "' + UTC_TIME + '","DeviceID": "' + DeviceID + '",\
    "ConfID": 13,"Queue": [{"Priority": 64,"LocSenEn": true,"RampDOWN": 200}]}}']

coap_burn_hour = ['1/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ConfID":3,"Queue":[{"Priority":7,"BurningHourTimeout":4}]}}']

coap_temp_hyst = ['1/1', '{"Cfg":{"UTC": "' + UTC_TIME + '","DeviceID": "' + DeviceID + '",\
    "ConfID": 3,"Queue": [{"Priority": 6,"TemperatureTimeHyst": 2}]}}']

coap_temp_max = ['1/1', '{"Cfg":{"UTC": "' + UTC_TIME + '","DeviceID": "' + DeviceID + '",\
    "ConfID": 3,"Queue": [{"Priority": 6,"SPHighTemperature": 40}]}}']

coap_st_up_seq_time = ['1/1', '{"Cfg":{"UTC": "' + UTC_TIME + '","DeviceID": "' + DeviceID + '",\
    "ConfID": 3,"Queue": [{"Priority":6,"StartUpSequenceTime": 20}]}}']

coap_cont_empty_Q = ['1/1', '{"Cfg":{"UTC": "' + UTC_TIME + '","DeviceID": "' + DeviceID + '",\
    "ConfID": 3,"Queue": []}}']

############  Lamp datapoints -> 2/1 , 2/2, .... , 2/8  ############
#coap_dim_curve = ['2/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
#    "ConfID":1,"Queue":[{"Priority":64,"Dimming_curve":[100,100,100000,2,0,1111,1444,0.5,"1-10V",1,0,10,8]}]}}']

coap_def_sw_prf_empty_Q = ['2/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ConfID":10,"Queue":[]}}']  ###### ConfID 10 or 14?? check data explorer

coap_def_sw_prf_w_sensor = ['2/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ConfID":14,"Queue":[{"Priority":64,"LightLvl":100}, {"Priority":67,"LightLvl":0,"Valid":{"Set":["[1.1.AmbientLightStatus]"]}}] }}']

coap_sw_prf_empty_Q = ['2/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ConfID":14,"Queue":[]}}']

coap_sw_prf_w_sensor = ['2/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ConfID":14,"Queue":[{"Priority":128,"LightLvl":100},{"Priority":131,"LightLvl":20},\
    {"Priority":134,"LightLvl":100,"Valid":{"Set":["[8.1.SensorValue]","0","!=","[8.2.SensorValue]","0","!=","||"]}}]}}']

coap_man_dim_com_empty_Q = ['2/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":""' + DeviceID + '"",\
    "ContID":1,"Queue":[]}}']

coap_man_dim_com = ['2/1', '{"Ctrl":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ContID":1,"Queue":[{"Priority":191,"LightLvl":0,"Valid":{"Set":["[3.1.LocDateTime]",\
    "{' + UTC_TIME_plus_5min + '}","<="]}}]}}']

############  Time/Pos datapoints -> 3/1  ############
coap_set_gps = ['3/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ConfID": 11,"Queue":[{"Priority":64,"GPSConfLAT":' + gps_lat + '},\
    {"Priority":64,"GPSConfLON":' + gps_long +'}]}}']

coap_day_sav_time = ['3/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ConfID": 11,"Queue":[[{"Priority": 64,"DaylightSavingDelta": 1, "Valid":\
    {"Set":["[3.1.LocDateTime]","{0 0 3 29 3 * 2020}",">=","[3.1.LocDateTime]","{0 59 2 25 10 * 2020}","<","&&"]}}]]}}']

############  Comms datapoints -> 5/1  ############

coap_rconf_trig = ['5/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ConfID":3,"Queue":[{"Priority":5,"DNS_ServerIP":"' + DNS_serverIP + '"},\
    {"Priority": 5,"APNconf":["' + apn_url + '","' + apn_usr + '","' + apn_pwd + '"]},\
    {"Priority": 5, "Rconf": ["' + RURL + '.1", "", ""]},\
    {"Priority": 5, "ReregistrationParameters": [1, 10, 15]}]}}']

coap_rconf = ['5/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ConfID":3,"Queue":[{"Priority":5,"DNS_ServerIP":"' + DNS_serverIP + '"},\
    {"Priority": 5,"APNconf":["' + apn_url + '","' + apn_usr + '","' + apn_pwd + '"]},\
    {"Priority": 5, "Rconf": ["' + RURL + '.1", "", ""]},]}}']

coap_sconf = ['5/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ConfID":2,"Queue":[{"Priority":64,"APNconf":["' + apn_url + '","' + apn_usr + '","' + apn_pwd + '"]},\
    {"Priority":64,"Sconf":["' + SURL + '.1","",""]},{"Priority":64,"CelOps":["auto",0]}]}}']

############  Management datapoints -> 7/1  ############
coap_fw_upd_apns = ['7/1', '{"Cfg":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ConfID": 6,"Queue": [{"Priority": 64,"UpdInf": ["' + apn_url + '", "' + apn_usr + '", "' + apn_pwd + '",\
    "http://' + RURL + '.2:8001/' + fw_upd_server + '", "", ""]}]}}']

coap_fw_notif_server = ['7/1', '{"Cfg":{"UTC":"' + UTC_TIME + '",\
    "ConfID":6,"Queue":[{"Priority":64,"UpdateNotifServer":\
    ["http://' + RURL + '.2:8001/' + fw_upd_notif_server + '"]}]}}']

coap_trig_upd = ['7/1', '{"Ctrl":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ContID":8,"Queue":[{"Priority":128,"UpdateApp":"' + fw_undertest + '"}]}}']

coap_trig_fake_upd = ['7/1', '{"Ctrl":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ContID":8,"Queue":[{"Priority":128,"UpdateApp":"' + fw_fake + '"}]}}']

coap_trig_dwgd = ['7/1', '{"Ctrl":{"UTC":"' + UTC_TIME + '","DeviceID":"' + DeviceID + '",\
    "ContID":8,"Queue":[{"Priority":128,"UpdateApp":"' + fw_current + '"}]}}']


############  Management datapoints -> 7/1  ############


## coap 8/1, {"Cfg": {"UTC": "210218114158","ConfID": 13,"DeviceID": "0013a20041bca3fa","Queue": [{"Priority": 64,"SensorGeneral": ["0000000000000000","local",0,20,false]}]}}
## coap 8/1, {"Cfg": {"UTC": "210218114158","ConfID": 13,"DeviceID": "0013A20041BC46C2","Queue": [{"Priority": 64,"SensorGeneral": ["0013a20041bca3fa","OW3",0,40,false]}]}}













