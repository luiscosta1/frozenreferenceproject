####################################################################################
# Filename:     resources_g3datapoints.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   05.03.2021
# Last update:  05.03.2021
#
# Version:      0.1
#
# Filetype:     Resource file
# Description:  Datapoint resources to support test scripts for the G3 project
# Status:       Under development (created from 210204_GDE6035Q_DataPoints_3_xx_xx_xx.xlsx)
# Limitation:   File must be in the same location as scripts
####################################################################################

max_lamp_instances = 8
max_sensor_instances = 32

# Controller Datapoints
Controller = [  # Information Points
    'DeviceID',
    'LastReset',
    'SN',
    'ProductKey',
    'AmbientLevel',
    'AmbientLightStatus',
    'TotalControllerRuntime',
    'TotalRuntimeMins',
    'QueueWdTS',
    'McuWatchDogResetTS',
    'Runtime',
    'CurrentTemp',
    'MaxTemp',
    'MaxTempTime',
    'FlightModeCount',
    'FlightMode',
    'RFIDID',
    'RFIDData',
    'Configured',
    'LocalSensorStatus',
    'InstallationDate',
    'DimLineStatus',
    'DimMeasured',
    'PWMsetting',
    'DimOutErr',
    'DinCorFact',
    'DALIStatus',
    'DALICount',
    'LGDriverStatus',
    'LGDriverErrors',
    'LSCount',
    'STM32FlashStatus',
    'STM32Voltage',
    'STM32Status',
    'TimeFromAppStart',
    'FactNewDone',
    'CauseOfRestart',
    'DebugInfo',
    'STFailCount',
    'JDBFailCount',
    'GPIO23FailCount',
    'PrevBurningPktTime',
    'ErrTemperatureHigh',
    'StartUpSequenceExpired',
    'DefaultOnTime',
    # Configurable Datapoints
    'TempLimit',
    'PhotocellOnAt',
    'PhotocellOffAt',
    'PhotocellOnLuxHoldTime',
    'PhotocellOffLuxHoldTime',
    'PhotocellMode',
    'PhotocellCalibration',
    'LocSenEn',
    'LocInhTim',
    'LocMaxFreq',
    'LocBRange',
    'RampUP',
    'RampDOWN',
    'DALIRelOp',
    'DebugEnable',
    'XbeeEnable',
    'StartUpSequenceTime',
    'BurningHourTimeout',
    'temperatureSamplingInterval',
    'TemperatureTimeHyst',
    'SPHighTemperature'
]

# Lamp Datapoints
Lamp = [  # Information Points
    'FeedbackDIMLevel',
    'BrokenLamp',
    'TotalRuntime',
    'Runtime',
    'LampTypeIP',
    'RunMins',
    'ControlDIMLevel',
    'WarmupActive',
    'ControlDIMLevel',
    'DALIType',
    'DALIBallastStatus',
    'DALI102MinPhysicalDIMLEVEL',
    'DALI102 GINCode'
    'DALI102FirmwareVersion',
    'DALI102SerialNumber',
    'DALI101_Error',
    'ControlDIMLevel',
    'DALILEDfailure',
    'DALI207_LEDType',
    'DALI207_LEDMode',
    'DALISwitchStatus',
    'DALISwitchType',
    'LampValidation',
    'LampREG',
    'DimFeedback',
    # Configurable Datapoints
    'OversizeFactor',
    'MaintenanceFactor',
    'MaintenanceInterval',
    'LowWattMinDim',
    'LowWattMaxDim',
    'HighWattMinDim',
    'HighWattMaxDim',
    'PowerFactorLimit',
    'WarmupTime',
    'LampType',
    'Active',
    'BrokenLampInhibitTimeOutInMS',
    'MinVoltage',
    'MinPower',
    'MinLumen',
    'MaxVoltage',
    'DALIindex',
    'MinLumen',
    'DALIFadeTime',
    'DALIFadeRate'
]

# PositionTime Datapoints
PositionTime = [  # Information Points
    'DateTime',
    'LocDateTime',
    'POSLON',
    'POSLAT',
    'POSHAC',
    'POSDAT',
    'ActualPOSLON',
    'ActualPOSLAT',
    'ActualPosHAC',
    'GPSFixStatus',
    'ActualGPStime',
    'Elevation',
    'AstroValid',
    'LastSendLat',
    'LastSendLon',
    'LastSendHac',
    'MoveDetect',
    'LastPosUpdate',
    'Seconds',
    'TimeSynTS',
    'TimeFromPOR',
    # Configurable Datapoints
    'Timezone',
    'DaylightSavingDelta'
    'GPSPosChange',
    'GPSConfLAT',
    'GPSConfLON',
    'NTP'
]

# ACPower Datapoints
ACPower = [  # Information Points
    'Relais',
    'ErrNoLoad',
    'ErrPWRLow',
    'EnergyMeter',
    'ACPower',
    'ACCurrent',
    'ACVolt',
    'ACPF',
    'ACFREQ',
    'ACRMSLoad',
    'ErrPWRHigh',
    'ErrPWRPF',
    'ErrSAGDetected',
    'ErrZXMissing',
    'SAGcount',
    'ZXMissingCount',
    'DurationPowerOutage',
    'ErrVoltageLow',
    'ErrVoltageHigh',
    'CntVoltageLow',
    'CntVoltageHigh',
    'DeltaEnergy',
    'IntMinP',
    'IntMaxP',
    # Configurable Datapoints
    'RelaisONDelay',
    'RelaisOffDelay',
    'SAG%Voltage',
    'ACPowerCalibration',
    'LowWatt',
    'HighWatt',
    'SPLowVoltage',
    'SPHighVoltage',
    'VoltageTimeHyst',
    'EnergyUpdatePeriod'
]

# Comms Datapoints
Comms = [  # Information Points
    'CommsType',
    'TotalRetry',
    #GSM & Zigbee:
    'ZB_Role',
    'ZB_OP',
    'ZB_OC',
    'ZB_EP',
    'ZB_LA',
    'ZB_AI',
    'ProtErrCnt',
    'IPv4',
    'IPv6',
    'IMSI',
    'IMEI',
    'ErrNTP',
    'ErrDNS',
    'CurOperator',
    'LastDimSendTS',
    'LastValidTS',
    'RegnState',
    'LastSuccessCommTS',
    'RegIP',
    'ProIP',
    'BadIPAddressResetDelay',
    # Configurable Datapoints
    'APN',
    'APNUser',
    'APNPW',
    'SURL',
    'Suser',
    'Spw',
    'CommsType',
    'Port',
    # GSM & Zigbee:
    'LRFCE',
    'LRFPANID',
    'LRFSC',
    'LRFII',
    'LRFEE',
    'LRFNK',
    'LRFKY',
    'LRFAP'
    'LRF_gateway_start_delay',
    'LRF_gateway_period',
    'LRF_is_gateway',
    'BinaryCompression',
    'PreferredOperatorMode',
    'PrefOperator',
    'BlockTransfer',
    # Registration
    'PingInterval',
    'RegnEnabled',
    'RegnRetryCount',
    'RegnRetryInterval',
    'RAPN',
    'RAPNUser',
    'RAPNPW',
    'RSURL',
    'RSuser',
    'RSpw',
    'IsDefaultRAPN',
    'OwletDNS_IP'
]

# DataInfo Datapoints
# Unused

# Management Datapoints
Management = [  # Information Points
    'FW-LUCO',
    'FW-MCU',
    'FW-Beta',
    'XBeeVR',
    'XBeeHV',
    'Gemalto',
    'UpdateAppBackUp',
    'UpdStatusCode',
    'UpdateServerIP',
    'FWVersionChanged',
    'IsDefaultUpdateNotifServer',
    'UpdateNotifServer',
    'UpdateNotifServerIP',
    # Configurable Datapoints
    'UpdAPN',
    'UpdAPNUsr',
    'UpdAPNPwd',
    'UpdURL',
    'UpdUsr',
    'UpdPwd',
    'IsDefaultUpdateConf'
]

# Sensor Datapoints
Sensor = [  # Information Points
    'SensorValue',
    'SensorCount',
    'BinDAT',
    'Frame',
    'PID',
    'CID',
    'SEP',
    'DEP',
    # Configurable Datapoints
    'SensorID'
    'SensorType'
    'SensorDefValue',
    'SensorHoldTime',
    'SenFDLsuo',
    'NodeID',
    'Profile',
    'Cluster',
    'SourceEP',
    'DestEP'
]


# CLDriver Datapoints
CLDriver = [  # Information Points
    'DriverType',
    'FWVersion',
    'Model',
    'HWVersion',
    'BarCode',
    'OutCurrent',
    'MaxOutCurrent',
    'OutPower',
    'MaxOutPow',
    'TotalEnergyCons'
    'LedTemp',
    'LedMinTemp',
    'LedMaxTemp',
    'DriverTemp',
    'DriverMinTemp',
    'DriverMaxTemp',
    'ThermalExceedOnLED',
    'ThermalExceedOnDrv',
    # Configurable Datapoints
    'Tstart',
    'Tstop',
    'Tmax',
    'TDimLevel',
    'ModWorkHour',
    'MaxOutCurHigh',
    'MaxOutCurLow',
    'Dimming profile'
]
