####################################################################################
# Filename:     test_unit_read_firmware_version.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   35.03.2021
# Last update:  03.03.2021
#
# Version:      0.1
#
# Filetype:     Test Case file
# Description:  Reads Firmware version from the device
# STATUS:       Stable
# Limitation:
####################################################################################

# local cli.py library already imports serial library
from cli import *
from resources_generic import *


# @pytest.mark.skip
def test_unit_read_firmware_version():
    """Requests and verifies if Firmware version is the expected"""
    cli_command = 'version'
    write_to_cli(cli_command)
    response = read_from_cli()
    assert fw_current in response
    print(response)
