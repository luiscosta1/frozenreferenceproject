####################################################################################
# Filename:     resources_generic.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   08.03.2021
# Last update:  08.03.2021
#
# Version:      0.1
#
# Filetype:     Resource file
# Description:  Variables to support test scripts for the Any project
# Status:       Under development
# Limitation:   File must be in the same location as scripts
####################################################################################

# Device Settings
DeviceID		= '0013A20041D03DC4'
#DeviceID		= '43bfc488-b8ec-4ab7-931f-4ef534131632'
RFID			= 'E00401009F5A3E6E'
fw_current     	= '3.32.15.100.dbg'
fw_undertest	= '3.32.15.101.dbg'
fw_fake	        = '3.32.15.101.fake'


# Reg Server Settings
RURL_dev		= '10.100.162'
RURL_tst		= '10.100.98'
RURL_prod		= '10.100.34'
# RURL is the one used in the code. To change between environments, change here!
RURL = RURL_tst

# Project Server Settings
SURL_dev		= '10.100.162'
SURL_tst		= '10.100.98'
SURL_prod		= '10.100.34'
# SURL is the one used in the code. To change between environments, change here!
SURL = SURL_tst

# APN Settings

apn_url = 'global.we.vp'
apn_usr = 'orange'
apn_pwd = 'orange'

DNS_serverIP = '195.177.188.21'

fw_upd_server = 'api/v1/G3/fw'
fw_upd_notif_server = 'api/v1/G3/fw'

gps_nova_sbe_lat = '38.67824'
gps_nova_sbe_long = '-9.32672'
gps_lat = gps_nova_sbe_lat
gps_long = gps_nova_sbe_long

# DM UI Settings
dm_ui_dev_url   = 'hyp-shared-dev-we-device-management-api.azurewebsites.net'
dm_ui_tst_url   = 'hyp-shared-tst-we-device-management-api.azurewebsites.net'
dm_ui_prod_url  = 'hyp-shared-prod-we-device-management-api.azurewebsites.net'
# dm_ui_url is the one used in the code. To change between environments, change here!
dm_ui_url  = dm_ui_tst_url

# DM UI HUBs
dm_ui_dev_hub   = 'hyp-shared-dev-we-iot-hub-01'
dm_ui_tst_hub   = 'hyp-shared-tst-we-iot-hub-01'
dm_ui_prod_hub  = 'hyp-shared-prd-we-iot-hub-01'
# dm_ui_hub is the one used in the code. To change between environments, change here!
dm_ui_hub  = dm_ui_tst_hub

# DM UI Bearer Tokens
dm_ui_tst_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyIsImtpZCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyJ9.eyJhdWQiOiJhcGk6Ly81MWFhYTUzYy04OTNiLTQ3NzctYTA3Ni0wMWExZGJkM2EyODQiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC80NzBmOTg0MS00ZDUxLTRjZjEtYWRiZS01MDc3ZmE4YzdlZGEvIiwiaWF0IjoxNjI4ODYyNTEzLCJuYmYiOjE2Mjg4NjI1MTMsImV4cCI6MTYyODg2NjQxMywiYWNyIjoiMSIsImFpbyI6IkFWUUFxLzhUQUFBQVBsK1lnWXkyVTgyajRLLy9MUTNqWFBraEs0cWJQTWxiMXJ4U3JDaERHVXB4REt4SG4wV3gyaU15SW91enhsYnczSGcyQTQ4NW55YWQySi9UbHloYXF4NnlvWDVqWHJNZ2t5b005L0RXTHhRPSIsImFtciI6WyJwd2QiLCJtZmEiXSwiYXBwaWQiOiJlNDViNzg2OC01MGFhLTRiYzQtYWE3ZS1iYTJlYmM3NDE2NmIiLCJhcHBpZGFjciI6IjAiLCJkZXZpY2VpZCI6ImJjZWQxODZhLTk4NTYtNGZkNi1iMTczLTJkZDI5YmMyNDhhZSIsImZhbWlseV9uYW1lIjoiQ09TVEEiLCJnaXZlbl9uYW1lIjoiTHXDrXMiLCJpcGFkZHIiOiI4Mi4xNTQuNjAuMzUiLCJuYW1lIjoiQ09TVEEgTHXDrXMiLCJvaWQiOiI2MjVhYjZiMS02NzM5LTQ3ODQtODM5NS04YTA0ZjY4MDM2MDIiLCJvbnByZW1fc2lkIjoiUy0xLTUtMjEtMzMxOTA5Njk2Ny0yODQ3ODA1MDE5LTE0NDE0Mzg5NzUtMTQxNzU5IiwicmgiOiIwLkFZRUFRWmdQUjFGTjhVeXR2bEIzLW94LTJtaDRXLVNxVU1STHFuNjZMcngwRm11QkFJUS4iLCJzY3AiOiJhY2Nlc3MiLCJzdWIiOiJEdVlGdW5uc3hIcDRvZVl0S21xOE90WjNxMmNGWi1hdGFTR2JXNjJYbmRrIiwidGlkIjoiNDcwZjk4NDEtNGQ1MS00Y2YxLWFkYmUtNTA3N2ZhOGM3ZWRhIiwidW5pcXVlX25hbWUiOiJhZG1sY29zdGFAc2NocmVkZXIuY29tIiwidXBuIjoiYWRtbGNvc3RhQHNjaHJlZGVyLmNvbSIsInV0aSI6IjEwRktEVW16c0VTcUxETy1mNDd0QXciLCJ2ZXIiOiIxLjAifQ.OS5A7rYNDuEHVyyRgrREfyRy07bC2-EEKmjGgBGe3Cs_ohQRB5LP_DcIuFgV8Cky1Plk8tgFr5TF4z01zxvu_EdbZYS-KzMUaES1JCOukBjEfGiwD2y1DKVpxHzJ0Vi9K3Y7cUapc3h4AQP8V3lcaXNW_hd0Mn4wObKClDGk7djiYNnSYLNkGy2oySDgLTCXaB89lTcD-RsJ6d_0xeO5SUefce2J2V9e6sdSAUGVOZeN4YQpAQKbXTIVyhinvUNstmE0Ju7eMp-21AQPaDnvStFfiXyZOX6Hw_uDEWr8s9JD1EJu809BQvY0lOj7duhHP_CJxBqHwG4Ike-8evUYVg'
dm_ui_token = dm_ui_tst_token

# Exedra Settings:
exedra_tst_url = 'hyperion.tst-schreder-exedra.com'
exedra_url = exedra_tst_url

# Exedra Bearer Tokens: Token Must be created in Exedra -> Settings -> Personal access Tokens
exedra_tst_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWY5ZmZjY2ZkZDAyNGMwMDFlYmI2MzE3IiwidGVuYW50IjoiSHlwZXJpb24iLCJ0b2tlbklkIjoiNDQ0MWRjYjgtMjllOC00ZTAzLTg1NTgtZGQwNjJlZDY1OWUyIiwiaWF0IjoxNjA3MzY2MDM4fQ.7OHYbE15seAO1tezfgVoZ9l5eqG0_l_BjEbdT2ABZ9E'
exedra_token = exedra_tst_token

# Jira Settings - Currently Hardcoded. Must Be requested to the user when running the program
jira_url = 'https://owlet-de.atlassian.net'
jira_user = 'lcosta@schreder.com'
jira_token = 'vy1IOpICNeVy9uzzaNKFD29E'