####################################################################################
# Filename:     http_requests.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   05.03.2021
# Last update:  08.03.2021
#
# Version:      0.1
#
# Filetype:     Library file
# Description:  Functions to support HTTP requests to DM UI
# STATUS:       New
# Limitation:   Need to change the request functions to be more generic
#               Eventually create a function for Post, another for request.... to be evaluated
####################################################################################

import requests
import json
from resources_generic import *

def dm_ui_request(url, request_type, payload=''):
    """
    Performs requests to DM UI API
    Returns the response from the server
    """

    headers = {
        # Fetch dm_ui_token from device-hubs request
        # Host: hyp-shared-tst-we-device-management-api.azurewebsites.net
        "Accept": "application / json, text / plain, * / *",
        "Authorization": "Bearer " + dm_ui_token,
        "Content-Type":  "application/json",
    }

    query = {
        'query': 'query'
    }

    response = requests.request(
        request_type,
        url,
        headers=headers,
        params=query,
        data=payload
    )

    return response


def dm_ui_request_device_id(device_id=DeviceID) -> bool:
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns True if the device is found
    """

    url = 'https://' + dm_ui_url + '/api/v1/device-hubs/' + dm_ui_hub + \
          '/devices?pageSize=10&pageIndex=0&searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = dm_ui_request(url, 'GET')

    # check if device ID is present in response
    if device_id in response.text:
        return True

    return False


def dm_ui_request_device_properties(parameter, device_id=DeviceID) -> str:
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns the device Firmware version on IoT HUB
    """

    url = 'https://' + dm_ui_url + '/api/v1/device-hubs/' + dm_ui_hub + \
          '/devices?pageSize=10&pageIndex=0&searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = dm_ui_request(url, 'GET')

    # check if device ID is not present in response
    if device_id not in response.text:
        return ''

    # device is present in IoT HUB, but response is complex. Needs further processing:
    response = get_value_from_response(response,parameter)

    return response


def get_value_from_response(response, value) -> str:
    """
    Receives the data from a device in Json format
    Returns the desired parameter defined in "value"
    """

    # Use Json library to fetch tenant from the response

    data_dict = json.loads(response.text)  # data_dict is created as a Python dictionary from the Json text

    payload_dict = data_dict['values'][0]   # we care only about the "values" dictionary

    return payload_dict[value]  # and we only want to return the desired word. eg.'firmwareVersion', 'tenant', etc

# INFO:
# response.text provides the following info (list of 3 dictionaries):
# {"count":1,
#  "nextPageToken":null,
#  "values":[{"id":"0013A20041BC46C2","tenant":"Manual Registration",
#  "firmwareVersion":"3.32.15.100","type":"G3","dimmingType":"1-10V","latitude":38.67824,"longitude":-9.326725,
#  "firmwareUpdateStatus":null,"firmwareUpdateOperationId":null,"assetId":"E00401009F5A3E6E","status":"Unreachable",
#  "lastConnectedDateTime":"2021-02-24T17:42:50.424+00:00",
#  "lastCommandAcknowledgedDateTime":"2021-02-24T17:42:50.424+00:00","assetStatus":"Valid"}]}


def dm_ui_send_coap_to_device(payload, device_id=DeviceID):  # payload must come from above
    """
    Posts a Coap Message to a specific device
    By default, device_id is the DeviceID under test
    Returns the response from the request
    """

    url = 'https://' + dm_ui_url + '/api/v1/devices/G3/' + device_id + '/command/custom'

    response = dm_ui_request(url, 'POST', payload)

    # response shall contain the status code (202 - OK)
    return response


# UNUSED
# currently replaced by dm_ui_request_device_properties()
def dm_ui_request_device_firmware(device_id=DeviceID) -> str:
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns the device Firmware version on IoT HUB
    """

    url = 'https://' + dm_ui_url + '/api/v1/device-hubs/' + dm_ui_hub + \
          '/devices?pageSize=10&pageIndex=0&searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = dm_ui_request(url, 'GET')

    # check if device ID is not present in response
    if device_id not in response.text:
        return ''

    # device is present in IoT HUB, but response is complex. Needs further processing:
    fw_read = get_value_from_response(response,'firmwareVersion')

    return fw_read


# def get_fw_version_from_response(response) -> str:
#     """
#     Receives the data from a device in Json format
#     Returns the extracted Firmware version
#     """
#
#     # Use Json library to fetch firmwareVersion from the response
#
#     data_dict = json.loads(response.text)  # data_dict is created as a Python dictionary from the Json text
#
#     payload_dict = data_dict['values'][0]   # we care only about the "values" dictionary
#
#     return payload_dict['firmwareVersion']  # and we only want to return the "firmwareVersion" word


# UNUSED
# currently replaced by dm_ui_request_device_properties()
def dm_ui_request_device_tenant(device_id=DeviceID) -> str:
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns the tenant where the device belongs
    """

    url = 'https://' + dm_ui_url + '/api/v1/device-hubs/' + dm_ui_hub + \
          '/devices?pageSize=10&pageIndex=0&searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = dm_ui_request(url, 'GET')

    # check if device ID is not present in response
    if device_id not in response.text:
        return ''

    # device is present in IoT HUB, but response is complex. Needs further processing:
    tenant = get_value_from_response(response, 'tenant')

    return tenant


# def get_tenant_from_response(response) -> str:
#     """
#     Receives the data from a device in Json format
#     Returns the extracted Firmware version
#     """
#
#     # Use Json library to fetch tenant from the response
#
#     data_dict = json.loads(response.text)  # data_dict is created as a Python dictionary from the Json text
#
#     payload_dict = data_dict['values'][0]   # we care only about the "values" dictionary
#
#     return payload_dict['tenant']  # and we only want to return the "tenant" word


# UNUSED
# currently replaced by dm_ui_request_device_properties()
def dm_ui_request_device_dimmingtype(device_id=DeviceID) -> str:
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns the dimming type of the device
    """

    url = 'https://' + dm_ui_url + '/api/v1/device-hubs/' + dm_ui_hub + \
          '/devices?pageSize=10&pageIndex=0&searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = dm_ui_request(url, 'GET')

    # check if device ID is not present in response
    if device_id not in response.text:
        return ''

    # device is present in IoT HUB, but response is complex. Needs further processing:
    dimmingtype = get_value_from_response(response, 'dimmingType')

    return dimmingtype


# UNUSED
# currently replaced by dm_ui_request_device_properties()
def dm_ui_request_device_latitude(device_id=DeviceID) -> str:
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns the latitude of the device
    """

    url = 'https://' + dm_ui_url + '/api/v1/device-hubs/' + dm_ui_hub + \
          '/devices?pageSize=10&pageIndex=0&searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = dm_ui_request(url, 'GET')

    # check if device ID is not present in response
    if device_id not in response.text:
        return ''

    # device is present in IoT HUB, but response is complex. Needs further processing:
    latitude = get_value_from_response(response, 'latitude')

    return latitude


# UNUSED
# currently replaced by dm_ui_request_device_properties()
def dm_ui_request_device_longitude(device_id=DeviceID) -> str:
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns the longitude of the device in IoT HUB
    """

    url = 'https://' + dm_ui_url + '/api/v1/device-hubs/' + dm_ui_hub + \
          '/devices?pageSize=10&pageIndex=0&searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = dm_ui_request(url, 'GET')

    # check if device ID is not present in response
    if device_id not in response.text:
        return ''

    # device is present in IoT HUB, but response is complex. Needs further processing:
    longitude = get_value_from_response(response, 'longitude')

    return longitude


# UNUSED
# currently replaced by dm_ui_request_device_properties()
def dm_ui_request_device_asset_id(device_id=DeviceID) -> str:
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns the asset ID of the device in IoT HUB
    """

    url = 'https://' + dm_ui_url + '/api/v1/device-hubs/' + dm_ui_hub + \
          '/devices?pageSize=10&pageIndex=0&searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = dm_ui_request(url, 'GET')

    # check if device ID is not present in response
    if device_id not in response.text:
        return ''

    # device is present in IoT HUB, but response is complex. Needs further processing:
    asset_id = get_value_from_response(response, 'assetId')

    return asset_id


# UNUSED
# currently replaced by dm_ui_request_device_properties()
def dm_ui_request_device_asset_status(device_id=DeviceID) -> str:
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns the asset status of the device in IoT HUB
    """

    url = 'https://' + dm_ui_url + '/api/v1/device-hubs/' + dm_ui_hub + \
          '/devices?pageSize=10&pageIndex=0&searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = dm_ui_request(url, 'GET')

    # check if device ID is not present in response
    if device_id not in response.text:
        return ''

    # device is present in IoT HUB, but response is complex. Needs further processing:
    asset_status = get_value_from_response(response, 'assetStatus')

    return asset_status


# UNUSED
# currently replaced by dm_ui_request_device_properties()
def dm_ui_request_device_status(device_id=DeviceID) -> str:
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns the Device status in IoT HUB
    """

    url = 'https://' + dm_ui_url + '/api/v1/device-hubs/' + dm_ui_hub + \
          '/devices?pageSize=10&pageIndex=0&searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = dm_ui_request(url, 'GET')

    # check if device ID is not present in response
    if device_id not in response.text:
        return ''

    # device is present in IoT HUB, but response is complex. Needs further processing:
    device_status = get_value_from_response(response, 'status')

    return device_status


