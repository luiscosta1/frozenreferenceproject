####################################################################################
# Filename:     resources_generic.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   08.03.2021
# Last update:  08.03.2021
#
# Version:      0.1
#
# Filetype:     Resource file
# Description:  Variables to support test scripts for the Any project
# Status:       Under development
# Limitation:   File must be in the same location as scripts
####################################################################################

# Device Settings
DeviceID		= '0013A20041BBADD0'
RFID			= 'E00401009F5A3E6E'
fw_current     	= '3.32.15.100.dbg'
fw_undertest	= '3.32.15.101.dbg'
fw_fake	        = '3.32.15.101.fake'


# Reg Server Settings
RURL_dev		= '10.100.162'
RURL_tst		= '10.100.98'
RURL_prod		= '10.100.34'
# RURL is the one used in the code. To change between environments, change here!
RURL = RURL_tst

# Project Server Settings
SURL_dev		= '10.100.162'
SURL_tst		= '10.100.98'
SURL_prod		= '10.100.34'
# SURL is the one used in the code. To change between environments, change here!
SURL = SURL_tst

# APN Settings

apn_url = 'global.we.vp'
apn_usr = 'orange'
apn_pwd = 'orange'

DNS_serverIP = '195.177.188.21'

fw_upd_server = 'api/v1/G3/fw'
fw_upd_notif_server = 'api/v1/G3/fw'

gps_nova_sbe_lat = '38.67824'
gps_nova_sbe_long = '-9.32672'
gps_lat = gps_nova_sbe_lat
gps_long = gps_nova_sbe_long

# DM UI Settings
dm_ui_dev_url   = 'hyp-shared-dev-we-device-management-api.azurewebsites.net'
dm_ui_tst_url   = 'hyp-shared-tst-we-device-management-api.azurewebsites.net'
dm_ui_prod_url  = 'hyp-shared-prod-we-device-management-api.azurewebsites.net'
# dm_ui_url is the one used in the code. To change between environments, change here!
dm_ui_url  = dm_ui_tst_url

# DM UI HUBs
dm_ui_dev_hub   = 'hyp-shared-dev-we-iot-hub-01'
dm_ui_tst_hub   = 'hyp-shared-tst-we-iot-hub-01'
dm_ui_prod_hub  = 'hyp-shared-prd-we-iot-hub-01'
# dm_ui_hub is the one used in the code. To change between environments, change here!
dm_ui_hub  = dm_ui_tst_hub

# DM UI Bearer Tokens
dm_ui_tst_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyIsImtpZCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyJ9.eyJhdWQiOiJhcGk6Ly81MWFhYTUzYy04OTNiLTQ3NzctYTA3Ni0wMWExZGJkM2EyODQiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC80NzBmOTg0MS00ZDUxLTRjZjEtYWRiZS01MDc3ZmE4YzdlZGEvIiwiaWF0IjoxNjE2NjY3MjIxLCJuYmYiOjE2MTY2NjcyMjEsImV4cCI6MTYxNjY3MTEyMSwiYWNyIjoiMSIsImFpbyI6IkFWUUFxLzhUQUFBQVlxN1ZRTlRsZFNjUmhCeWlNZVZuMGlCalF4MEo4SXZXOW1NZTZGMVFUaURvTzZEeWI2VStIYkZOSW9lN0VYaklQV1k2WEpEclk4eTFDWjRteHVRK0V2MXJXMnlEQUp2ZlJ1YUI1VzBZYVBRPSIsImFtciI6WyJwd2QiLCJtZmEiXSwiYXBwaWQiOiJkMjIwYjhhYi0xZmY5LTRjOWUtYTlmZS04MDIwNGVhMjg5NzQiLCJhcHBpZGFjciI6IjAiLCJkZXZpY2VpZCI6ImJjZWQxODZhLTk4NTYtNGZkNi1iMTczLTJkZDI5YmMyNDhhZSIsImZhbWlseV9uYW1lIjoiQ09TVEEiLCJnaXZlbl9uYW1lIjoiTHXDrXMiLCJpcGFkZHIiOiI4Mi4xNTQuMTE0LjIyMiIsIm5hbWUiOiJDT1NUQSBMdcOtcyIsIm9pZCI6IjYyNWFiNmIxLTY3MzktNDc4NC04Mzk1LThhMDRmNjgwMzYwMiIsIm9ucHJlbV9zaWQiOiJTLTEtNS0yMS0zMzE5MDk2OTY3LTI4NDc4MDUwMTktMTQ0MTQzODk3NS0xNDE3NTkiLCJyaCI6IjAuQVlFQVFaZ1BSMUZOOFV5dHZsQjMtb3gtMnF1NElOTDVINTVNcWY2QUlFNmlpWFNCQUlRLiIsInNjcCI6ImFjY2VzcyIsInN1YiI6IkR1WUZ1bm5zeEhwNG9lWXRLbXE4T3RaM3EyY0ZaLWF0YVNHYlc2MlhuZGsiLCJ0aWQiOiI0NzBmOTg0MS00ZDUxLTRjZjEtYWRiZS01MDc3ZmE4YzdlZGEiLCJ1bmlxdWVfbmFtZSI6ImFkbWxjb3N0YUBzY2hyZWRlci5jb20iLCJ1cG4iOiJhZG1sY29zdGFAc2NocmVkZXIuY29tIiwidXRpIjoiRHhiSDNFeTBNa21nZXVVWERYdHFBQSIsInZlciI6IjEuMCJ9.XUeNgQk9Cec36UthUhEhkkaOb-7OPFG8VNfN02lTwqyGUdn-8SuVWUx4trCb9k0kP6VjQ0gA_RJhbFA9JtfjdHlCobhFmp03nv0qVXLK1158OldEskDzKbcNYPPJtj6RqTq_r_TcaLGHF3JUJPZzksyyBx8Oh23V9cjV8BgmPPk5OoxbReptKq9C8sBIvs2wlgWQj8qWfvXI-_qR5hhDB_B_bjk3gzmY7QiWWbQoMeS7_gByALxYENVy6HVln_czLjI1-oFAgRF76eJznWaVJXPgxKYvX28pBFtkXaZdsyggbY06P2tKuI1Qu83FHb80QUuR_Oafpv_OlT0GNGKpEA'
dm_ui_token = dm_ui_tst_token

# Exedra Settings:
exedra_tst_url = 'hyperion.tst-schreder-exedra.com'
exedra_url = exedra_tst_url

# Exedra Bearer Tokens: Token Must be created in Exedra -> Settings -> Personal access Tokens
exedra_tst_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWY5ZmZjY2ZkZDAyNGMwMDFlYmI2MzE3IiwidGVuYW50IjoiSHlwZXJpb24iLCJ0b2tlbklkIjoiNDQ0MWRjYjgtMjllOC00ZTAzLTg1NTgtZGQwNjJlZDY1OWUyIiwiaWF0IjoxNjA3MzY2MDM4fQ.7OHYbE15seAO1tezfgVoZ9l5eqG0_l_BjEbdT2ABZ9E'
exedra_token = exedra_tst_token

# Jira Settings - Currently Hardcoded. Must Be requested to the user when running the program
jira_url = 'https://owlet-de.atlassian.net'
jira_user = 'lcosta@schreder.com'
jira_token = 'vy1IOpICNeVy9uzzaNKFD29E'