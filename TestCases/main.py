####################################################################################
# Filename:     main.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   20.03.2021
# Last update:  25.03.2021
#
# Version:      0.1
#
# Filetype:     Main Application
# Description:  Main loop. Links the low level python functions to HTML GUI
# STATUS:       New
# Limitation:   On exit, settings are not being saved for next session
####################################################################################


import eel
import platform
from cli import *
from resources_coap import *
from resources_generic import *
from resources_g3specific import *
from resources_g3datapoints import *
from http_requests import *
from http_requests_azure import *
from http_requests_jira import *
from http_requests_exedra import *
from coap_cli import *
from coap_e2e import *
from datapoints import *
import resources_generic

# This variable means the DM UI Token has been successfully fetched from the credentials
g_is_online = False
g_environment = 'test'


def start_eel():
    """
    This Function starts the GUI and keeps Python running in Background
    """

    page = 'index.html'
    eel.init("gui")

    try:
        # Tries to open app in Chrome
        eel.start(page, mode='chrome')
    except EnvironmentError:
        # If Chrome isn't found, fallback to Microsoft Edge on Win10 or greater
        if sys.platform in ['win32', 'win64'] and int(platform.release()) >= 10:
            eel.start(page, mode='edge', block=False)
        else:
            raise

    while True:
        # Loop to keep Python running
        print('\nPython is running....')
        eel.sleep(2)


@eel.expose
def set_environment(env):
    """
    This function sets the required Python variables, according to what the user chooses in the GUI
    """
    global g_environment

    if env == "test":
        resources_generic.RURL = RURL_tst
        resources_generic.SURL = SURL_tst
        resources_generic.dm_ui_url = dm_ui_tst_url
        g_environment = "test"
        print("Setting Environment to Test")
    elif env == "development":
        resources_generic.RURL = RURL_dev
        resources_generic.SURL = SURL_dev
        resources_generic.dm_ui_url = dm_ui_dev_url
        g_environment = "development"
        print("Setting Environment to Development")

    return


@eel.expose
def validate_credentials(gui_username, gui_password):
    """
    This function validates if the provided credentials from the GUI are valid
    Returns True if the DM UI bearer Token is successfully obtained
    Returns False otherwise
    """
    global g_is_online
    # if credentials are valid
    #if get_azure_ad_token(gui_username, gui_password):
    if True:  # replace this True by the get_azure_ad_token return value
        global_is_online = True
        return g_is_online
    else:
        return False

@eel.expose
def is_online():
    """This function retrieves the Online status to the GUI"""
    global g_is_online
    return g_is_online

@eel.expose
def get_environment():
    """This function retrieves the current environment status to the GUI"""
    global g_environment
    return g_environment


@eel.expose
def get_device_id():
    """This function retrieves the current DeviceID to the GUI"""
    print('returning device ID ', resources_generic.DeviceID, ' to GUI')
    return resources_generic.DeviceID


@eel.expose
def set_device_id(gui_device_id):
    """This function sets the Device ID received from the GUI
    Returns False if input string is invalid"""

    # Need to verify the input values - Length == 16 and Alphanumerical. Return False if NOK
    if len(gui_device_id) != 16:
        print('Invalid Device ID length: "', gui_device_id, '" -> discarding ....')
        return False
    elif not gui_device_id.isalnum():
        print('Invalid Device ID string: "', gui_device_id, '" -> discarding ....')
        return False

    resources_generic.DeviceID = gui_device_id
    print('Setting DeviceID ', resources_generic.DeviceID, ' from GUI')
    return True


@eel.expose
def get_rfid():
    """This function retrieves the current RFID to the GUI"""
    print('returning RFID ', resources_generic.RFID, ' to GUI')
    return resources_generic.RFID


@eel.expose
def set_rfid(gui_rfid):
    """This function sets the RFID received from the GUI
    Returns False if input string is invalid"""

    # Need to verify the input values - Length == 16 and Alphanumerical. Return False if NOK
    if len(gui_rfid) != 16:
        print('Invalid RFID length: "', gui_rfid, '" -> discarding ....')
        return False
    elif not gui_rfid.isalnum():
        print('Invalid RFID string: "', gui_rfid, '" -> discarding ....')
        return False

    resources_generic.RFID = gui_rfid
    print('Setting RFID ', resources_generic.RFID, ' from GUI')
    return True


@eel.expose
def get_fw_current():
    """This function retrieves the current firmware to the GUI"""
    print('returning fw_current ', resources_generic.fw_current, ' to GUI')
    return resources_generic.fw_current


@eel.expose
def set_fw_current(gui_fw_current):
    """This function sets the current Firmware received from the GUI
    Returns False if input string is invalid"""

    # Need to verify the input values TODO: how to validate FW version?
    # if not gui_fw_current.isalnum():
    #     print('Invalid fw_current : "', gui_fw_current, '" -> discarding ....')
    #     return False

    resources_generic.fw_current = gui_fw_current
    print('Setting fw_current ', resources_generic.fw_current, ' from GUI')
    return True


@eel.expose
def get_fw_undertest():
    """This function retrieves the firmware under test to the GUI"""
    print('returning fw_undertest ', resources_generic.fw_undertest, ' to GUI')
    return resources_generic.fw_undertest


@eel.expose
def set_fw_undertest(gui_fw_undertest):
    """This function sets the firmware under test received from the GUI
    Returns False if input string is invalid"""

    # Need to verify the input values TODO: how to validate FW version?
    # if not gui_fw_current.isalnum():
    #     print('Invalid fw_current : "', gui_fw_current, '" -> discarding ....')
    #     return False

    resources_generic.fw_undertest = gui_fw_undertest
    print('Setting fw_undertest ', resources_generic.fw_undertest, ' from GUI')
    return True


@eel.expose
def get_gps_lat():
    """This function retrieves the gps latitude  to the GUI"""
    print('returning gps_lat ', resources_generic.gps_lat, ' to GUI')
    return resources_generic.gps_lat


@eel.expose
def set_gps_lat(gui_lat):
    """This function sets the gps latitude received from the GUI
    Returns False if input value is not convertible to float"""

    # Need to verify the input values. Return False if NOK
    try:
        f_gui_lat = float(gui_lat)
    except ValueError:
        print ('Invalid GPS Latitude: "', gui_lat, '" -> discarding ....')
        return False

    resources_generic.gps_lat = round(f_gui_lat, 6)  # round to 6 decimal places
    print('Setting gps_lat ', resources_generic.gps_lat, ' from GUI')
    return True


@eel.expose
def get_gps_long():
    """This function retrieves the gps longitude  to the GUI"""
    print('returning gps_long ', resources_generic.gps_long, ' to GUI')
    return resources_generic.gps_long


@eel.expose
def set_gps_long(gui_long):
    """This function sets the gps longitude received from the GUI
    Returns False if input value is not convertible to float"""

    # Need to verify the input values. Return False if NOK
    try:
        f_gui_long = float(gui_long)
    except ValueError:
        print ('Invalid GPS Longitude: "', gui_long, '" -> discarding ....')
        return False

    resources_generic.gps_long = round(f_gui_long, 6)  # round to 6 decimal places
    print('Setting gps_long ', resources_generic.gps_long, ' from GUI')
    return True


@eel.expose
def save_current_settings():
    """This function retrieves the gps longitude  to the GUI"""
    # TODO: Implement with Easy Settings
    print('returning gps_long ', resources_generic.gps_long, ' to GUI')
    return resources_generic.gps_long


if __name__ == '__main__':
    import sys

    start_eel()

