####################################################################################
# Filename:     test_unit_coap_message_via_cli.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   09.03.2021
# Last update:  09.03.2021
#
# Version:      0.1
#
# Filetype:     Test Case file
# Description:  Test Cases to write coap messages to the g3 cli
# STATUS:       Stable
# Limitation:
####################################################################################

from coap_cli import *
from datapoints import *
from resources_coap import *

@pytest.mark.unit
def test_send_coap_factory_default():
    success = send_g3_coap_factory_default()

    assert success, "G3 factory Default failed"
    pass


@pytest.mark.unit
def test_send_coap_firmware_upd_settings():
    """
    Sends the firmware update url and firmware Update notification servers configuration via G3 CLI Coap
    Verifies the correct values are stored in the datapoints
    """
    send_g3_firmware_update_settings()

    # Validate the Datapoints assume the correct values
    response = read_datapoint('UpdateNotifServer')
    expected_response = 'http://' + RURL + '.2:8001/' + fw_upd_notif_server
    assert response == expected_response

    response = read_datapoint('UpdAPN')
    expected_response = 'global.we.vp'
    assert response == expected_response

    response = read_datapoint('UpdAPNUsr')
    expected_response = 'orange'
    assert response == expected_response

    response = read_datapoint('UpdAPNPwd')
    expected_response = 'orange'
    assert response == expected_response

    response = read_datapoint('UpdURL')
    expected_response = 'http://' + RURL + '.2:8001/' + fw_upd_server
    assert response == expected_response

    response = read_datapoint('UpdUsr')
    expected_response = ''
    assert response == expected_response

    response = read_datapoint('UpdPwd')
    expected_response = ''
    assert response == expected_response

    pass


@pytest.mark.unit
def test_write_send_coap_sensor_config_all():   # Still needs to be tested!

    sensor_id = '0013a20041bca3fa'
    sensor_type = 'OW3'
    sensor_def_value = 0
    senFDLsuo = 'true'

    # write sensor configurations via Coap
    for instance in range(1, max_sensor_instances+1):
        sensor_hold_time = instance  # for test purposes, set the hold time equal to the sensor instance
        send_g3_coap_sensor_conf(sensor_id, sensor_type, sensor_def_value, sensor_hold_time, senFDLsuo, instance)

    # read sensor configurations via datapoints
    for instance in range(1, max_sensor_instances + 1):
        response = read_datapoint('SensorID')
        assert response == '0013a20041bca3fa', "Written sensor config does not match datapoint!"
        response = read_datapoint('SensorType')
        assert response == 'OW3', "Written sensor config does not match datapoint!"
        response = read_datapoint('SensorDefValue')
        assert response == '0', "Written sensor config does not match datapoint!"
        response = read_datapoint('SensorHoldTime')
        assert response == str(instance), "Written sensor config does not match datapoint!"
        response = read_datapoint('SenFDLsuo')
        assert response == 'true', "Written sensor config does not match datapoint!"

    pass


@pytest.mark.unit
def test_write_send_coap_dimming_curve_all():  # Not Finalized! Still needs to be tested!

    for instance in range(1, max_lamp_instances+1):
        send_g3_coap_dimming_curve(instance)

    # read sensor configurations via datapoints
    for instance in range(1, max_lamp_instances + 1):
        response = read_datapoint('SensorID')
        assert response == '0013a20041bca3fa', "Written Lamp config does not match datapoint!"

    pass
