####################################################################################
# Filename:     g3_d2c_message_builder.py.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   08.04.2021
# Last update:  27.04.2021
#
# Version:      2.0
#
# Filetype:     Function file
# Description:  Functions required for G3 D2C message building
# STATUS:       Stable
# Limitation:
####################################################################################

import config


def build_g3_d2c_message(queue, deviceID, deviceIP):
    """
    This function receives the desired information Queue and encapsulates it into the G3 message body template
    Returns the full message body to be sent on the payload to Rabbit MQ
    """

    g3_message_body = '{ ' \
          '  \\"IP\\": \\"' + deviceIP + ':4096\\",' \
          '  \\"Time\\":\\"' + config.now_Time + '\\",' \
          '  \\"Type\\":\\"information\\",' \
          '  \\"Header\\": {\\"Content-Format\\":\\"application/json\\",\\"Content-Type\\":\\"application/json\\"},' \
          '  \\"RequestDomain\\":\\"' + config.request_domain + '\\",' \
          '  \\"Payload\\": { ' \
          '            \\"Info\\":{' \
          '                    \\"UTC\\":\\"' + config.now_UTC + '\\",' \
          '                    \\"Queue\\":[' \
                      + queue + \
                      '                    ],' \
          '                    \\"DeviceID\\":\\"' + deviceID + '\\"' \
          '                    }' \
          '            },  ' \
          '  \\"DeviceId\\":       \\"' + deviceID + '\\", ' \
          '  \\"Compression\\":    \\"json_long\\", ' \
          '  \\"CompressionName\\":\\"JSON long\\", ' \
          '  \\"ProductType\\":    \\"3A-2213-2100-A238-1111\\", ' \
          '  \\"ProductTypeName\\":\\"G3\\"' \
          ' }'

    return g3_message_body


def build_g3_d2c_queue(queue_list, deviceID):
    """"
    This function receives a list of parameters to be put in the queue
    TODO: Add Sensor Count Messages. Add Ballast status messages
    Returns the queue as a string to be sent inside the G3 message to the Cloud
    """

    queue = ''

    # for parameter in queue_list[0]:
    for parameter in queue_list:
        # check if it's an ACPower_errors_list parameter
        if parameter in config.ACPower_errors_dict:
            parameter_value = config.ACPower_errors_dict[parameter]
            queue += '{ \\"Config\\":0,' \
                     '  \\"DeltaTime\\":\\"00:01:00\\",' \
                     '  \\"Source\\":\\"ACPower\\",' \
                     '  \\"' + parameter + '\\":' + str(parameter_value).lower() + '},'

        if parameter == 'BrokenLamp_1':
            queue += '{ \\"Config\\":0,' \
                     '  \\"DeltaTime\\":\\"00:01:00\\",' \
                     '  \\"Source\\":\\"Lamp\\",' \
                     '  \\"BrokenLamp\\":' + str(config.BrokenLamp).lower() + ',' \
                     '  \\"Index\\":1},'

        if parameter == 'BrokenLamp_2':
            queue += '{ \\"Config\\":0,' \
                     '  \\"DeltaTime\\":\\"00:01:00\\",' \
                     '  \\"Source\\":\\"Lamp\\",' \
                     '  \\"BrokenLamp\\":' + str(config.BrokenLamp).lower() + ',' \
                     '  \\"Index\\":2},'

        if parameter == 'Dimfeedback_1':
            queue += '{\\"DeltaTime\\":\\"00:01:00\\",' \
                     ' \\"Priority\\":0,' \
                     ' \\"Source\\":\\"Lamp\\",' \
                     ' \\"DimFeedback\\": [ ' \
                     + config.dimfeedback_dict["EnergyMeter"] + ', ' \
                     + config.dimfeedback_dict["ACPower"] + ', ' \
                     + config.dimfeedback_dict["ACCurrent"] + ', ' \
                     + config.dimfeedback_dict["ACVoltage"] + ', ' \
                     + config.dimfeedback_dict["ACPowerFactor"] + ', ' \
                     + config.dimfeedback_dict["FeedbackDIMLevel"] + ', ' \
                     + config.dimfeedback_dict["MinACPower"] + ', ' \
                     + config.dimfeedback_dict["MaxACPower"] + ' ' \
                                                               '], ' \
                     ' \\"Index\\":1},'

        if parameter == 'Dimfeedback_2':
            queue += '{\\"DeltaTime\\":\\"00:01:00\\",' \
                     ' \\"Priority\\":0,' \
                     ' \\"Source\\":\\"Lamp\\",' \
                     ' \\"DimFeedback\\": [ ' \
                     + config.dimfeedback_dict["EnergyMeter"] + ', ' \
                     + config.dimfeedback_dict["ACPower"] + ', ' \
                     + config.dimfeedback_dict["ACCurrent"] + ', ' \
                     + config.dimfeedback_dict["ACVoltage"] + ', ' \
                     + config.dimfeedback_dict["ACPowerFactor"] + ', ' \
                     + config.dimfeedback_dict["FeedbackDIMLevel"] + ', ' \
                     + config.dimfeedback_dict["MinACPower"] + ', ' \
                     + config.dimfeedback_dict["MaxACPower"] + ' ' \
                                                               '], ' \
                     ' \\"Index\\":2},'

        if parameter == 'EnergyReading':
            queue += '{\\"DeltaTime\\":\\"00:01:00\\",' \
                     ' \\"Config\\":0, ' \
                     ' \\"EnergyReading\\": [ ' \
                     + config.dimfeedback_dict["EnergyMeter"] + ', ' \
                     + config.dimfeedback_dict["ACPower"] + ', ' \
                     + config.dimfeedback_dict["ACCurrent"] + ', ' \
                     + config.dimfeedback_dict["ACVoltage"] + ', ' \
                     + config.dimfeedback_dict["ACPowerFactor"] + '] , ' \
                     ' \\"Source\\":\\"ACPower\\"},'

        if parameter == 'BurningHours':
            queue += '{ \\"Priority\\":1,' \
                     '  \\"Source\\":\\"Controller\\",' \
                     '  \\"BurningHours\\": {' \
                     '  \\"TotalRunTime\\": [' \
                            + config.BurningHours_dict["TotalControllerRuntime"] + ',' \
                            + config.BurningHours_dict["TotalRuntime"] + ' ] } },'

        if parameter == 'Verification':
            queue += '{ \\"Verification\\": [' \
                     '  \\"' + deviceID + '\\"]  },'

        if parameter == 'LampReg':
            queue += '{ \\"Source\\":\\"Lamp\\",' \
                     '  \\"Index\\":1,' \
                     '  \\"LampREG\\": [' \
                     '  \\"LED\\", 156,\\"123456789999\\",\\"011B\\",\\"23174068\\"] },' \

    # After the For cycle above, the last element will have an extra comma. Remove it:
    queue = queue[:-1]

    return queue

