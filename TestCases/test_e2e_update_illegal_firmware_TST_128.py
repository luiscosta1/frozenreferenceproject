####################################################################################
# Filename:     test_e2e_update_illegal_firmware_TST_128.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   06.04.2021
# Last update:  06.04.2021
#
# Version:      0.1
#
# Filetype:     Test Case file
# Description:  Firmware update Tests
# STATUS:       Stable - TODO: Trigger update remotely via DM UI (fw_undertest)
# Limitation:
####################################################################################

from coap_cli import *
from datapoints import *
from http_requests import *
from logs import *


@pytest.mark.e2e
def test_e2e_update_illegal_firmware_via_local_coap():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    # read initial firmware version on the device
    initial_fw_device = read_datapoint('FW-LUCO')

    success = send_g3_firmware_update_settings()
    assert success, "Failed to write firmware update settings via cli coap"
    logger.info("Firmware update and Update Notification Server successfully written via CLI Coap")

    # send update app coap command
    success = send_g3_coap_update_app(coap_trig_fake_upd)
    assert success, "Failed to send firmware update via cli coap"
    logger.info("Update App command successfully written via CLI Coap")

    # wait 10 mins
    logger.info("Waiting 10mins until trying to re-open CLI")
    time.sleep(10 * 60)

    cli_ready = wait_g3_cli_ready
    assert cli_ready, "Unable to re-open CLI"
    logger.info("CLI successfully re-opened")

    # read firmware version on the device
    final_fw_device = read_datapoint('FW-LUCO')
    # read firmware version via DM Ui http request
    fw_cloud = dm_ui_request_device_firmware(DeviceID)

    logger.info("Initial Firmware on the device: " + initial_fw_device)
    logger.info("Final Firmware on the device: " + final_fw_device)
    logger.info("Firmware on the Device Manager: " + fw_cloud)

    # On an illegal firmware change, all the above values shall match
    assert initial_fw_device in final_fw_device, "Firmware on the device was not supposed to change"
    assert final_fw_device in fw_cloud, "Firmware on the device does not match the server"

    pass
