####################################################################################
# Filename:     http_requests_exedra.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   15.03.2021
# Last update:  25.03.2021
#
# Version:      0.1
#
# Filetype:     Library file
# Description:  Functions to support HTTP requests to Exedra Platform
# STATUS:       New
# Limitation:
#
####################################################################################

import requests
from requests.auth import HTTPBasicAuth
import json
from resources_generic import *


# https://hyperion.tst-schreder-exedra.com/documentation/core-apis

# Add Exedra Personal Access Token to the Resources_generic

# rework this function to work with Exedra


def exedra_request(url, request_type, payload=''):
    """
    Performs requests to Exedra API
    Returns the response from the server
    """

    headers = {
        "Accept": "application / json, text / plain, * / *",
        "Authorization": "Bearer " + exedra_token
    }

    query = {
        'query': 'query'
    }

    response = requests.request(
        request_type,
        url,
        headers=headers,
        # params=query
    )

    return response


def exedra_request_device_id(device_id=DeviceID) -> bool:
    """
    Requests a search for a specific Device ID in Exedra.
    Returns True if the device is found
    """

    # url = 'https://' + exedra_url + '/api/v2/devices?limit=500&devicetypemodel=Schreder%20G3%20[LightPoint]&field=configuration.deviceId'
    url = 'https://' + exedra_url + '/api/v1/devices?queryLang=ObjectName%20%3D%20'
    #url = 'https://' + exedra_url + '/api/v2/devices/'

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = exedra_request(url, 'GET')

    # check if device ID is present in response
    if device_id in response.text:
        return True

    return False


# def exedra_request_devices(device_id) -> bool:
#     """
#     Requests the device list in Exedra.
#     Returns True if the device is found
#     """
#     # This is not returning All Devices! Need to search the device directly!
#     url = 'https://' + exedra_url + '/api/v2/devices?devicetypemodel=Schreder%20G3%20[LightPoint]&field=configuration.deviceId'
#
#     # Append the desired device_id to the end of the URL requested
#     # url += device_id
#
#     response = exedra_request(url, 'GET')
#
#     # check if device ID is present in response
#     if device_id in response.text:
#         return True
#
#     return False


def exedra_request_device_properties(device_id, parameter) -> str:
    """
    Requests a search for a specific Device ID in Exedra.
    Returns the information about the device
    """

    # url = 'https://' + exedra_url + '/api/v2/devices?limit=500&devicetypemodel=Schreder%20G3%20[LightPoint]&field=configuration.deviceId'
    url = 'https://' + exedra_url + '/api/v1/devices?queryLang=ObjectName%20%3D%20'

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = exedra_request(url, 'GET')

    if device_id not in response.text:
        return ''

    response = get_value_from_exedra_response(response, parameter)

    return response


def get_value_from_exedra_response(response, parameter) -> str:
    """
    Receives the data from a device in Json format
    Returns the desired parameter defined in "value"
    """

    # Use Json library to fetch tenant from the response

    data_list = json.loads(response.text)  # data_dict is created as a Python dictionary from the Json text

    # data_list is a list of several dictionaries, with different parameters, such as:
    # 'core', 'coords', 'attributes', 'failures', 'configuration', 'metering'

    # we need to check in which dictionary the requested parameter is located

    # data_list has strings, integers and dictionaries. We only need the dictionaries in this context

    list_dicts = [data_list[0]['core'], data_list[0]['coords'], data_list[0]['attributes'], data_list[0]['failures'],
                  data_list[0]['configuration'], data_list[0]['metering']]

    for dictionary in list_dicts:  # for every dictionary present in list_dicts
        parameter_value = dictionary.get(parameter)
        if parameter_value is None:
            continue
        else:
            break

    return parameter_value

    # payload_dict = data_list[0]['configuration']  # we care only about the "values" dictionary

    #return payload_dict[parameter]  # and we only want to return the desired word. eg.'firmwareVersion', 'tenant', etc




# Create function to send dimming level - Check Core APIs "Real Time Control"

# Create functions to retrieve Device information in Exedra, just like implemented in http_requests_azure.py
