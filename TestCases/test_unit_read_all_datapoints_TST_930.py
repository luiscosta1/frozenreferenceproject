####################################################################################
# Filename:     test_unit_read_all_datapoints_TST_930.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   05.03.2021
# Last update:  06.04.2021
#
# Version:      0.1
#
# Filetype:     Test Case file
# Description:  Unit Test Cases to read all device datapoints
# STATUS:       Need to assert the received values
# Limitation:
####################################################################################


from datapoints import *
from resources_g3datapoints import *
from logs import *


@pytest.mark.unit
def test_unit_read_Controller_datapoints():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    print('\n')
    for datapoint in Controller:    # Run through Controller datapoints defined in resources_g3datapoints.py

        response = read_datapoint(datapoint)
        print('Controller.' + datapoint + ' = ' + response)
        logger.info(datapoint + ' : ' + str(response))
        # assert response is not 'DATAPOINT NOT FOUND', "Datapoint not found!"
    pass


@pytest.mark.unit
def test_unit_read_Lamp_datapoints():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    print('\n')
    for datapoint in Lamp:          # Run through Lamp datapoints defined in resources_g3datapoints.py

        response = read_datapoint(datapoint)
        print('Lamp.1.' + datapoint + ' = ' + response)
        logger.info(datapoint + ' : ' + str(response))
        #assert response is not 'ERROR', "ERROR in Datapoint!"

    pass


@pytest.mark.unit
def test_unit_read_all_Lamp_datapoints():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    print('\n')
    for instance in range (1,max_lamp_instances):    # Run through all Sensor instances
        print('\n')
        for datapoint in Lamp:          # Run through Lamp datapoints defined in resources_g3datapoints.py

            response = read_datapoint(datapoint, instance)
            print('Lamp.', instance, '.', datapoint, ' = ', response)
            logger.info(datapoint + ' : ' + str(response))
            #assert response is not '', "Datapoint not found!"
            #assert response is not 'ERROR', "Not able to read datapoint!"

    pass


@pytest.mark.unit
def test_unit_read_PositionTime_datapoints():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    print('\n')
    for datapoint in PositionTime:          # Run through PositionTime datapoints defined in resources_g3datapoints.py

        response = read_datapoint(datapoint)
        print('PositionTime.' + datapoint + ' = ' + response)
        logger.info(datapoint + ' : ' + str(response))
        # assert response is not 'ERROR', "ERROR in Datapoint!"

    pass


@pytest.mark.unit
def test_unit_read_ACPower_datapoints():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    print('\n')
    for datapoint in ACPower:          # Run through ACPower datapoints defined in resources_g3datapoints.py

        response = read_datapoint(datapoint)
        print('ACPower.' + datapoint + ' = ' + response)
        logger.info(datapoint + ' : ' + str(response))
        assert response is not 'ERROR', "ERROR in Datapoint!"
    pass


@pytest.mark.unit
def test_unit_read_Comms_datapoints():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    print('\n')
    for datapoint in Comms:          # Run through Comms datapoints defined in resources_g3datapoints.py

        response = read_datapoint(datapoint)
        print('Comms.' + datapoint + ' = ' + response)
        logger.info(datapoint + ' : ' + str(response))
        # assert response is not '', "Datapoint not found!"
        assert response is not 'ERROR', "ERROR in Datapoint!"

    pass


@pytest.mark.unit
def test_unit_read_Management_datapoints():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    print('\n')
    for datapoint in Management:          # Run through Management datapoints defined in resources_g3datapoints.py

        response = read_datapoint(datapoint)
        print('Management.' + datapoint + ' = ' + response)
        logger.info(datapoint + ' : ' + str(response))
        # assert response is not '', "Datapoint not found!"
        # assert response is not 'ERROR', "ERROR in Datapoint!"

    pass


@pytest.mark.unit
def test_unit_read_Sensor_datapoints():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    print('\n')
    for datapoint in Sensor:          # Run through Sensor datapoints defined in resources_g3datapoints.py

        response = read_datapoint(datapoint)
        print('Sensor.' + datapoint + ' = ' + response)
        logger.info(datapoint + ' : ' + str(response))
        # assert response is not '', "Datapoint not found!"
        # assert response is not 'ERROR', "Not able to read datapoint!"

    pass


@pytest.mark.unit
def test_unit_read_all_Sensor_datapoints():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    print('\n')
    for instance in range(1, max_sensor_instances):  # Run through all Sensor instances
        print('\n')
        for datapoint in Sensor:                    # Run through Sensor datapoints defined in resources_g3datapoints.py

            response = read_datapoint(datapoint, instance)
            print('Sensor.', instance, '.', datapoint,  ' = ', response)
            logger.info(datapoint + ' : ' + str(response))
            # assert response is not '', "Datapoint not found!"
            # assert response is not 'ERROR', "Not able to read datapoint!"
    pass


@pytest.mark.unit
def test_unit_read_CLDriver_datapoints():

    # create log file
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    logger = create_log_file(log_filename)

    print('\n')
    for datapoint in CLDriver:          # Run through CLDriver datapoints defined in resources_g3datapoints.py

        response = read_datapoint(datapoint)
        print('CLDriver.' + datapoint + ' = ' + response)
        logger.info(datapoint + ' : ' + str(response))
        # assert response is not '', "Datapoint not found!"
        # assert response is not 'ERROR', "ERROR in Datapoint!"

    pass
