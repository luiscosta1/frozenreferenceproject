####################################################################################
# Filename:     resources_g3specific.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   02.03.2021
# Last update:  03.03.2021
#
# Version:      0.1
#
# Filetype:     Resource file
# Description:  Variables to support test scripts for the G3 project
# Status:       Under development
# Limitation:   File must be in the same location as scripts
####################################################################################


# ********   Dynamic Variables examples   ********
# import time
# CURRENT_TIME = time.asctime()       # timestamp like 'Thu Apr  6 12:45:21 2006'
# if time.localtime()[3] > 12:
# AFTERNOON = True
# else:
# AFTERNOON = False

# from datetime import datetime
# this sets CURRENT_TIME in YYMMDDhhmmss
# CURRENT_TIME = datetime.today().strftime('%Y%m%d%H%M%S') Need to set 2021 to only 21


# Serial Port Settings
port_cli_port = 'COM16'
port_logs_port = 'COM20'
port_binary_port = 'COM18'
port_baudrate = 115200
port_databits = 8
port_stopbits = 1
port_parity = 'None'
port_timeout = 1

serial_logs_max_characters = 500



