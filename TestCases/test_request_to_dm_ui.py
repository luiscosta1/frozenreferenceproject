####################################################################################
# Filename:     test_request_to_dm_ui.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   05.03.2021
# Last update:  22.03.2021
#
# Version:      0.1
#
# Filetype:     Test Case file
# Description:  Unit Test Cases to Request the Device info from Device Management
# STATUS:       New
# Limitation:
####################################################################################


from http_requests import *
from resources_generic import *
import json
import pytest


def test_dm_ui_request():
    """
    Requests All devices registered in TST IoT HUB

    Although the returned "counts" is a higher value, it only shows the first 100
    To request All devices, we may need to use the nextPageToken returned in the response"""

    url = "https://" + dm_ui_url + "/api/v1/device-hubs/" + dm_ui_hub + "/devices"

    response = dm_ui_request(url, 'GET')

    print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))


def test_dm_ui_request_device_id(device_id=DeviceID):
    """
    Checks if a given device is registered in TST IoT HUB
    """

    # device_id = '0013A20041BC46C2'

    device_present = dm_ui_request_device_id(device_id)

    assert device_present is True, "Device not Present in IoT HUB"

    print('\nDevice ', device_id, ' is present in IoT HUB!')


# @pytest.mark.skipif() shall be skipped if test_dm_ui_request_device_id fails
def test_dm_ui_request_device_firmware():
    """
    Checks if a given device is firmware in IoT HUB matches the expected
    """

    # device_id = '0013A20041BC46C2'

    # fw_read = dm_ui_request_device_firmware(device_id)
    fw_read = dm_ui_request_device_properties('firmwareVersion')

    assert fw_read == fw_undertest, "Firmware versions Mismatch!"

    print('\nDevice Firmware in IoT Hub: ', fw_read)


def test_dm_ui_request_device_tenant():
    """
    Checks the current tenant of the device
    """

    # device_id = '0013A20041BC46C2'

    #tenant = dm_ui_request_device_tenant(device_id)
    tenant = dm_ui_request_device_properties('tenant')

    print('\nDevice is in ', tenant, 'tenant')

    assert tenant == 'Nova-Lab', "Firmware versions Mismatch!"


def test_dm_ui_request_device_dimmingtype():
    """
    Checks the current dimming type of the device
    """

    # device_id = '0013A20041BC46C2'

    #dimmingtype = dm_ui_request_device_dimmingtype(device_id)
    dimmingtype = dm_ui_request_device_properties('dimmingType')

    print('\nDevice dimming type in IoT HUB: ', dimmingtype)

    assert dimmingtype == '1-10V', "Device Dimming Type Mismatch!"


def test_dm_ui_request_device_latitude():
    """
    Checks the current latitude of the device
    """

    # device_id = '0013A20041BC46C2'

    #latitude = dm_ui_request_device_latitude(device_id)
    latitude = dm_ui_request_device_properties('latitude')

    print('\nDevice latitude in IoT HUB: ', latitude)

    assert str(latitude) == gps_lat, "Latitude Mismatch!"


def test_dm_ui_request_device_longitude():
    """
    Checks the current longitude of the device
    """

    # device_id = '0013A20041BC46C2'

    #longitude = dm_ui_request_device_longitude(device_id)
    longitude = dm_ui_request_device_properties('longitude')

    print('\nDevice longitude in IoT HUB: ', longitude)

    assert str(longitude) == gps_long, "Longitude Mismatch!"


def test_dm_ui_request_device_asset_id():
    """
    Checks the current Asset ID of the device
    """

    # device_id = '0013A20041BC46C2'

    #asset_id = dm_ui_request_device_asset_id(device_id)
    asset_id = dm_ui_request_device_properties('assetId')

    print('\nDevice Asset in IoT HUB: ', asset_id)

    assert asset_id == RFID, "Device Asset Mismatch!"


def test_dm_ui_request_device_asset_status():
    """
    Checks the current Asset status of the device
    """

    # device_id = '0013A20041BC46C2'

    #asset_status = dm_ui_request_device_asset_status(device_id)
    asset_status = dm_ui_request_device_properties('assetStatus')

    print('\nDevice Asset Status IoT HUB: ', asset_status)

    assert asset_status == 'Valid', "Device Asset is invalid"


def test_dm_ui_request_device_status():
    """
    Checks the current Asset status of the device
    """

    #device_id = '0013A20041BC46C2'

    #device_status = dm_ui_request_device_status(device_id)
    device_status = dm_ui_request_device_properties('status')

    print('\nDevice Status IoT HUB: ', device_status)

    assert device_status == 'Reachable', "Device is not reachable!"

