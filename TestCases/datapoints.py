####################################################################################
# Filename:     datapoints.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   05.03.2021
# Last update:  03.03.2021
#
# Version:      0.1
#
# Filetype:     Library file
# Description:  Functions to support reading and writing G3 Datapoints
# STATUS:       New
# Limitation:   File must be in the same location as scripts
####################################################################################


from cli import *
from resources_g3datapoints import *


# to read and write datapoints messages to G3 device
# format: read dp G3_Controller.1.DeviceID
# functions herein shall call read and write functions from cli file


def read_datapoint(datapoint, instance=1) -> str:
    """
    Receives Datapoint to be read
    Returns response

    Info: Lamps contain max of 8 instances. Sensors contain max of 32 instances -> defined in resources_g3specific.py
    """

    obj = find_datapoint_in_g3_object(datapoint)

    if obj == '':  # datapoint not found in objects list
        print('Datapoint ' + datapoint + ' not found in objects list')
        return 'DATAPOINT NOT FOUND'

    # check validity of input parameters:
    if obj == 'Sensor':
        if instance > max_sensor_instances:
            print('Sensor cannot exceed ', max_sensor_instances, ' instances')
            return ''
    elif obj == 'Lamp':
        if instance > max_lamp_instances:
            print('Lamp cannot exceed ',  max_sensor_instances, ' instances')
            return ''

    # build command to send to device
    data = 'read dp ' + obj + '.' + str(instance) + '.' + datapoint

    write_to_cli(data)
    response = read_from_cli()

    return response


def write_datapoint(datapoint, value, instance=1) -> bool:
    """
    Writes Datapoint.      eg.: write dp Lamp.1.MinVoltage=1
    Returns True if Success (read OK from CLI after sending command)

    """

    obj = find_datapoint_in_g3_object(datapoint)

    # build command to send
    data = 'write dp ' + obj + '.' + str(instance) + '.' + datapoint + '=' + str(value)

    print(data)

    write_to_cli(data)

    response = read_from_cli()

    if response == 'OK':
        return True
    else:
        return False


def find_datapoint_in_g3_object(datapoint) -> str:
    """
    This is a auxiliary function, which
    Receives the datapoint to be read
    Returns the object which the datapoint belongs to
    eg. DeviceID datapoint belongs to G3_Controller object
    """

    # go through lists defined in g3datapoint_resources and return the object
    if datapoint in Controller:
        obj = 'Controller'
    elif datapoint in Lamp:
        obj = 'Lamp'
    elif datapoint in PositionTime:
        obj = 'PositionTime'
    elif datapoint in ACPower:
        obj = 'ACPower'
    elif datapoint in Comms:
        obj = 'Comms'
    elif datapoint in Management:
        obj = 'Management'
    elif datapoint in Sensor:
        obj = 'Sensor'
    elif datapoint in CLDriver:
        obj = 'CLDriver'
    else:
        obj = ''
        print('Datapoint ' + datapoint + ' Not found!')

    return obj
