####################################################################################
# Filename:     test_send_g3_D2C_messages.py.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   09.04.2021
# Last update:  27.04.2021
#
# Version:      2.0
#
# Filetype:     Test case file
# Description:  Tests Sending g3 device to Owlet Iot messages
# STATUS:       Stable
# Limitation:
####################################################################################

from g3_d2c_message_builder import *
from coapthon.client.helperclient import HelperClient


def test_exedra_coap():
    host = '40.74.34.91' # TST
    #host = '51.105.190.87' # DEV
    port = 5683
    path = 'information'
    msg = '{"Info":{"UTC":"210503065138","Queue":[{"Verification":["0013A20041774B90"]}],"DeviceID":"0013A20041774B90"}}'
    msg = '{"Info":{"UTC":"210509042946","Queue":[{"Config":0,"DeltaTime":"00:01:25","DALIStatus":[],"Source":"Controller"}],"DeviceID":"0013A2004157CE6C"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"210813120255","Queue":[{"DeltaTime":"00:01:55","Priority":0,"Source":"Lamp","DimFeedback":[0.18543473,1,0.011,224.9,1,32,0,2.1],"Index":1}],"DeviceID":"0013A20041D03DC4"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"210813120255","Queue":[{"BrokenLamp":true,"DeltaTime":"00:22:11","Priority":0,"Source":"Lamp","Index":1}],"DeviceID":"0013A20041D03DC4"},"RequestDomain":null,"Type":"information"}'

    client = HelperClient(server=(host, port))
    response = client.put(path, payload=msg, timeout=1)
    print(response)
    client.stop()

    return


def test_send_g3_all_errors():

    queue_list = ['BrokenLamp_1', 'BrokenLamp_2', 'ErrPWRPF', 'ErrVoltageHigh', 'ErrZXMissing',
                  'ErrPWRHigh', 'ErrVoltageLow', 'ErrSAGDetected', 'ErrPWRLow']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_all_ACPower_errors():

    queue_list = ['ErrPWRPF', 'ErrVoltageHigh', 'ErrZXMissing', 'ErrPWRHigh',
                  'ErrVoltageLow', 'ErrSAGDetected', 'ErrPWRLow']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_BrokenLamp_index_1():

    queue_list = ['BrokenLamp_1']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_BrokenLamp_index_2():

    queue_list = ['BrokenLamp_2']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_ErrPWRPF():

    queue_list = ['ErrPWRPF']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_ErrVoltageHigh():

    queue_list = ['ErrVoltageHigh']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_ErrZXMissing():

    queue_list = ['ErrZXMissing']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_ErrPWRHigh():

    queue_list = ['ErrPWRHigh']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_ErrVoltageLow():

    queue_list = ['ErrVoltageLow']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_ErrSAGDetected():

    queue_list = ['ErrSAGDetected']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_ErrPWRLow():

    queue_list = ['ErrPWRLow']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_Dimfeedback_1():

    queue_list = ['Dimfeedback_1']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_Dimfeedback_2():

    queue_list = ['Dimfeedback_2']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_EnergyReading():

    queue_list = ['EnergyReading']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_BurningHours():

    queue_list = ['BurningHours']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_Verification():

    queue_list = ['Verification']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_LampReg():

    queue_list = ['LampReg']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_EnergyReading_and_all_errors():

    queue_list = ['BrokenLamp_1', 'BrokenLamp_2', 'ErrPWRPF' , 'ErrVoltageHigh', 'ErrZXMissing',
                  'ErrPWRHigh', 'ErrVoltageLow', 'ErrSAGDetected', 'ErrPWRLow', 'EnergyReading']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_Verification_and_Dimfeedback():

    queue_list = ['Verification', 'Dimfeedback_1']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return


def test_send_g3_BurningHours_and_Dimfeedback():

    queue_list = ['BurningHours', 'Dimfeedback_1']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)
    # build the POST URL
    post_request_url = config.rabbit_mq_url + '/api/exchanges/' + config.vhost + "/worldInbox/publish"

    # build the payload
    message_body = build_g3_d2c_message(queue)

    # encapsulate the G3 message body inside the request payload
    payload = '{"properties":{},"routing_key":"information","payload":"' + message_body + '","payload_encoding":"string"}'

    # send the request
    response = rabbit_request(post_request_url, 'POST', payload)
    print('\n3 put_request_url response:\n', response, response.content)

    assert response.content

    return
