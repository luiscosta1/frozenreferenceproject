####################################################################################
# Filename: resources_g3datapoints.py
# Author: Luis Costa
# E-mail: lcosta@schreder.com
# Copyright: Schreder Hyperion
#
# Created On:   05.03.2021
# Last update:  05.03.2021
#
# Version:      0.1
#
# Filetype:     Resource file
# Description:  Datapoint resources to support test scripts for the G3 project
# Status:       Under development (created from 210204_GDE6035Q_DataPoints_3_xx_xx_xx.xlsx)
# Limitation:   File must be in the same location as scripts
####################################################################################


# Controller Datapoints
# information Points
Controller = ['DeviceID',
              'LastReset',
              'SN',
              'ProductKey',
              'AmbientLevel',
              'AmbientLightStatus',
              'TotalControllerRuntime',
              'TotalRuntimeMins',
              'QueueWdTS',
              'McuWatchDogResetTS',
              'Runtime',
              'CurrentTemp',
              'MaxTemp',
              'MaxTempTime',
              'FlightModeCount',
              'FlightMode',
              'RFIDID',
              'RFIDData',
              'Configured',
              'LocalSensorStatus',
              'InstallationDate',
              'DimLineStatus',
              'DimMeasured',
              'PWMsetting',
              'DimOutErr',
              'DinCorFact',
              'DALIStatus',
              'DALICount',
              'LGDriverStatus',
              'LGDriverErrors',
              'LSCount',
              'STM32FlashStatus',
              'STM32Voltage',
              'STM32Status',
              'TimeFromAppStart',
              'FactNewDone',
              'CauseOfRestart',
              'DebugInfo',
              'STFailCount',
              'JDBFailCount',
              'GPIO23FailCount',
              'PrevBurningPktTime',
              'ErrTemperatureHigh',
              'StartUpSequenceExpired',
              'DefaultOnTime']

# Lamp Datapoints

# PositionTime Datapoints

# ACPower Datapoints

# Comms Datapoints

# DataInfo Datapoints

# Management Datapoints

# Sensor Datapoints

# CLDriver Datapoints
